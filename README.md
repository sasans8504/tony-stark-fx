股票追蹤小程式
--

注意事項：強烈建議使用jre1.8.0_181以上的版本，其他版本雖然兼容，但會有不可預期的畫面表現。

99%使用java FX 製作

1%用於系統列(System tray)

希望可以讓對javaFX有興趣的人在這裡找到需要的功能或寫法

內容包含
--
簡易登入畫面 (Login Page)

按鈕指向表格並反白提醒 (Link to TableView then select row)

表格中包含一個以上的按鈕 (Buttons in TableView column)

~~使用Timeline做定時查詢~~ (並非開出新的執行緒，會對主執行緒造成影響)

設定場景的父方 (initOwner to parent)