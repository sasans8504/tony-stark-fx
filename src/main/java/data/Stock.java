package data;

import bin.Formula;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import net.sf.json.JSONObject;

import java.math.BigDecimal;

import static data.DataSystem.*;

public class Stock {
    private final SimpleStringProperty id;
    private final SimpleStringProperty name;
    private final SimpleStringProperty price_now;
    private final SimpleStringProperty price_ysclose;
    private final SimpleStringProperty price_open;
    private final SimpleStringProperty price_buy;
    private final SimpleStringProperty count;
    private final SimpleStringProperty buy_fee;
    private final SimpleStringProperty alarm_low;
    private final SimpleStringProperty alarm_high;
    private final SimpleBooleanProperty alarm;
    private JSONObject stock;

    public Stock(JSONObject stock) {
        this(stock.getString(DATA_ID),
                stock.getString(DATA_NAME),
                stock.optString(DATA_PRICE_YESTERDAY, "0"),
                stock.optString(DATA_PRICE_OPEN, "0"),
                stock.getString(DATA_PRICE_NOW),
                stock.getString(DATA_PRICE_BUY),
                stock.getString(DATA_COUNT),
                stock.optString(DATA_BUY_FEE, "0"),
                stock.getString(DATA_ALARM_LOW),
                stock.getString(DATA_ALARM_HIGH),
                stock.getBoolean(DATA_ALARM));
        this.stock = stock;
    }

    public Stock(String id, String name, String ysPrice, String openPrice, String nowPrice, String buyPrice,
                 String count, String buyFee, String alarm_low, String alarm_high, boolean alarm) {
        this.id = new SimpleStringProperty(id);
        this.name = new SimpleStringProperty(name);
        this.price_ysclose = new SimpleStringProperty(ysPrice);
        this.price_open = new SimpleStringProperty(openPrice);
        this.price_now = new SimpleStringProperty(nowPrice);
        this.price_buy = new SimpleStringProperty(buyPrice);
        this.count = new SimpleStringProperty(count);
        this.buy_fee = new SimpleStringProperty(buyFee);
        this.alarm_low = new SimpleStringProperty(alarm_low);
        this.alarm_high = new SimpleStringProperty(alarm_high);
        this.alarm = new SimpleBooleanProperty(alarm);
    }

    public void setWithType(String type, String value) {
        switch (type) {
            case DATA_ID:
                setId(value);
                break;
            case DATA_NAME:
                setName(value);
                break;
            case DATA_PRICE_NOW:
                setPrice_now(value);
                break;
            case DATA_PRICE_BUY:
                setPrice_buy(value);
                break;
            case DATA_COUNT:
                setCount(value);
                break;
        }
    }

    public void minusCount(String count){
        updateCount("-"+count);
    }

    public void updateCount(String count){
        int origin = Integer.parseInt(getCount());
        origin += Integer.parseInt(count);
        setCount(String.valueOf(origin));
    }

    public void updateBuyFee(String fee){
        if(getCount().equals("0")) {
            setBuy_fee("0");
            return;
        }
        BigDecimal origin = new BigDecimal(getBuy_fee());
        BigDecimal bdFee = new BigDecimal(fee);
        setBuy_fee(origin.subtract(bdFee).toPlainString());
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public Stock setId(String id) {
        this.id.set(id);
        return this;
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public Stock setName(String name) {
        this.name.set(name);
        return this;
    }

    public String getPrice_ysclose() {
        return price_ysclose.get();
    }

    public SimpleStringProperty price_yscloseProperty() {
        return price_ysclose;
    }

    public Stock setPrice_ysclose(String price_ysclose) {
        this.price_ysclose.set(price_ysclose);
        return this;
    }

    public String getPrice_open() {
        return price_open.get();
    }

    public SimpleStringProperty price_openProperty() {
        return price_open;
    }

    public Stock setPrice_open(String price_open) {
        this.price_open.set(price_open);
        return this;
    }

    public String getPrice_now() {
        return price_now.get();
    }

    public SimpleStringProperty price_nowProperty() {
        return price_now;
    }

    public Stock setPrice_now(String price_now) {
        this.price_now.set(price_now);
        return this;
    }

    public String getPrice_buy() {
        return price_buy.get();
    }

    public SimpleStringProperty price_buyProperty() {
        return price_buy;
    }

    public Stock setPrice_buy(String price_buy) {
        this.price_buy.set(price_buy);
        return this;
    }

    public String getCount() {
        return count.get();
    }

    public SimpleStringProperty countProperty() {
        return count;
    }

    public Stock setCount(String count) {
        this.count.set(count);
        return this;
    }

    public String getBuy_fee() {
        return buy_fee.get();
    }

    public SimpleStringProperty buy_feeProperty() {
        return buy_fee;
    }

    public Stock setBuy_fee(String buy_fee) {
        this.buy_fee.set(buy_fee);
        return this;
    }

    public String getAlarm_low() {
        return alarm_low.get();
    }

    public SimpleStringProperty alarm_lowProperty() {
        return alarm_low;
    }

    public Stock setAlarm_low(String alarm_low) {
        this.alarm_low.set(alarm_low);
        return this;
    }

    public String getAlarm_high() {
        return alarm_high.get();
    }

    public SimpleStringProperty alarm_highProperty() {
        return alarm_high;
    }

    public Stock setAlarm_high(String alarm_high) {
        this.alarm_high.set(alarm_high);
        return this;
    }

    public boolean isAlarm() {
        return alarm.get();
    }

    public SimpleBooleanProperty alarmProperty() {
        return alarm;
    }

    public Stock setAlarm(boolean alarm) {
        this.alarm.set(alarm);
        return this;
    }

    @Override
    public String toString() {
        stock = new JSONObject();
        stock.put(DATA_ID, getId());
        stock.put(DATA_NAME, getName());
        stock.put(DATA_COUNT, getCount());
        stock.put(DATA_PRICE_YESTERDAY, getPrice_ysclose());
        stock.put(DATA_PRICE_OPEN, getPrice_open());
        stock.put(DATA_PRICE_NOW, getPrice_now());
        stock.put(DATA_PRICE_BUY, getPrice_buy());
        stock.put(DATA_BUY_FEE, getBuy_fee());
        stock.put(DATA_ALARM, isAlarm());
        stock.put(DATA_ALARM_LOW, getAlarm_low());
        stock.put(DATA_ALARM_HIGH, getAlarm_high());
        return stock.toString();
    }
}
