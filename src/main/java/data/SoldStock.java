package data;

import bin.Formula;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import net.sf.json.JSONObject;

import java.time.LocalDate;

import static data.DataSystem.*;

public class SoldStock {
    private final SimpleStringProperty key;
    private final SimpleStringProperty id;
    private final SimpleStringProperty name;
    private final SimpleStringProperty price_market;
    private final SimpleStringProperty price_sold;
    private final SimpleStringProperty count;
    private final SimpleStringProperty sold_time;
    private final SimpleStringProperty buy_fee;
    private final SimpleStringProperty sold_fee;
    private final SimpleStringProperty tax;
    private final SimpleStringProperty revenue;
    private static ObservableList<SoldStock> soldStocks = FXCollections.observableArrayList();

    public SoldStock(String key, String id, String name, String price_market, String price_sold,
                     String count, String sold_time, String buy_fee, String sold_fee,
                     String tax, String revenue){
        this.key = new SimpleStringProperty(key);
        this.id = new SimpleStringProperty(id);
        this.name = new SimpleStringProperty(name);
        this.price_market = new SimpleStringProperty(price_market);
        this.price_sold = new SimpleStringProperty(price_sold);
        this.count = new SimpleStringProperty(count);
        this.sold_time = new SimpleStringProperty(sold_time);
        this.buy_fee = new SimpleStringProperty(buy_fee);
        this.sold_fee = new SimpleStringProperty(sold_fee);
        this.tax = new SimpleStringProperty(tax);
        this.revenue = new SimpleStringProperty(revenue);
    }

    private SoldStock(){
        this.key = new SimpleStringProperty(String.valueOf(System.currentTimeMillis()));
        this.id = new SimpleStringProperty("0");
        this.name = new SimpleStringProperty("none");
        this.price_market = new SimpleStringProperty("0");
        this.price_sold = new SimpleStringProperty("0");
        this.count = new SimpleStringProperty("0");
        this.sold_time = new SimpleStringProperty("0");
        this.buy_fee = new SimpleStringProperty("0");
        this.sold_fee = new SimpleStringProperty("0");
        this.tax = new SimpleStringProperty("0");
        this.revenue = new SimpleStringProperty("0");
    }

    public static SoldStock fromStock(Stock stock){
        SoldStock soldStock = new SoldStock();
        soldStock.setId(stock.getId());
        soldStock.setName(stock.getName());
        soldStock.setPrice_market(stock.getPrice_now());
        return soldStock;
    }

    public static SoldStock fromJson(JSONObject stock){
        return new SoldStock().setKey(stock.getString(DATA_KEY))
                .setId(stock.getString(DATA_ID))
                .setName(stock.getString(DATA_NAME))
                .setPrice_market(stock.getString(DATA_PRICE_MARKET))
                .setPrice_sold(stock.getString(DATA_PRICE_SOLD))
                .setCount(stock.getString(DATA_COUNT))
                .setSold_time(stock.getString(DATA_TIME_SOLD))
                .setBuy_fee(stock.getString(DATA_BUY_FEE))
                .setSold_fee(stock.getString(DATA_SOLD_FEE))
                .setTax(stock.getString(DATA_TAX))
                .setRevenue(stock.getString(DATA_REVENUE));
    }

    public static ObservableList<SoldStock> toItemFrom(String id, LocalDate from){
        return toItem(id, from, null);
    }

    public static ObservableList<SoldStock> toItemTo(String id, LocalDate to){
        return toItem(id, null, to);
    }

    public static ObservableList<SoldStock> toItem(String id){
        return toItem(id, null, null);
    }

    public static ObservableList<SoldStock> toItem(String id, LocalDate from, LocalDate to){
        soldStocks.clear();
        JSONObject historyStocks = loadStock(DATA_HISTORY_STOCK);
        for(Object key : historyStocks.keySet()){
            JSONObject stock = historyStocks.getJSONObject(key.toString());
            if(stock.getString(DATA_ID).equals(id)){
                if(from != null){
                    if(from.compareTo(LocalDate.parse(stock.getString(DATA_TIME_SOLD))) > 0){
                        continue;
                    }
                }
                if(to != null){
                    if(to.compareTo(LocalDate.parse(stock.getString(DATA_TIME_SOLD))) < 0){
                        continue;
                    }
                }
                soldStocks.add(fromJson(stock));
            }
        }
        return soldStocks;
    }

    public void sellInit(String soldPrice, String count,
                         String buyFee, String soldFee, String tax){
        setPrice_sold(soldPrice);
        setCount(count);
        setSold_time(LocalDate.now().toString());
        setBuy_fee(buyFee);
        setSold_fee(soldFee);
        setTax(tax);
        setRevenue(Formula.revenue(this));
    }

    public String getKey() {
        return key.get();
    }

    public SimpleStringProperty keyProperty() {
        return key;
    }

    public SoldStock setKey(String key) {
        this.key.set(key);
        return this;
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public SoldStock setId(String id) {
        this.id.set(id);
        return this;
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public SoldStock setName(String name) {
        this.name.set(name);
        return this;
    }

    public String getPrice_market() {
        return price_market.get();
    }

    public SimpleStringProperty price_marketProperty() {
        return price_market;
    }

    public SoldStock setPrice_market(String price_market) {
        this.price_market.set(price_market);
        return this;
    }

    public String getPrice_sold() {
        return price_sold.get();
    }

    public SimpleStringProperty price_soldProperty() {
        return price_sold;
    }

    public SoldStock setPrice_sold(String price_sold) {
        this.price_sold.set(price_sold);
        return this;
    }

    public String getCount() {
        return count.get();
    }

    public SimpleStringProperty countProperty() {
        return count;
    }

    public SoldStock setCount(String count) {
        this.count.set(count);
        return this;
    }

    public String getSold_time() {
        return sold_time.get();
    }

    public SimpleStringProperty sold_timeProperty() {
        return sold_time;
    }

    public SoldStock setSold_time(String sold_time) {
        this.sold_time.set(sold_time);
        return this;
    }

    public String getBuy_fee() {
        return buy_fee.get();
    }

    public SimpleStringProperty buy_feeProperty() {
        return buy_fee;
    }

    public SoldStock setBuy_fee(String buy_fee) {
        this.buy_fee.set(buy_fee);
        return this;
    }

    public String getSold_fee() {
        return sold_fee.get();
    }

    public SimpleStringProperty sold_feeProperty() {
        return sold_fee;
    }

    public SoldStock setSold_fee(String sold_fee) {
        this.sold_fee.set(sold_fee);
        return this;
    }

    public String getTax() {
        return tax.get();
    }

    public SimpleStringProperty taxProperty() {
        return tax;
    }

    public SoldStock setTax(String tax) {
        this.tax.set(tax);
        return this;
    }

    public String getRevenue() {
        return revenue.get();
    }

    public SimpleStringProperty revenueProperty() {
        return revenue;
    }

    public SoldStock setRevenue(String revenue) {
        this.revenue.set(revenue);
        return this;
    }

    public JSONObject toJson(){
        JSONObject jsob = new JSONObject();
        jsob.put(DATA_KEY, getKey());
        jsob.put(DATA_ID, getId());
        jsob.put(DATA_NAME, getName());
        jsob.put(DATA_PRICE_MARKET, getPrice_market());
        jsob.put(DATA_PRICE_SOLD, getPrice_sold());
        jsob.put(DATA_COUNT, getCount());
        jsob.put(DATA_TIME_SOLD, getSold_time());
        jsob.put(DATA_BUY_FEE, getBuy_fee());
        jsob.put(DATA_SOLD_FEE, getSold_fee());
        jsob.put(DATA_TAX, getTax());
        jsob.put(DATA_REVENUE, getRevenue());
        return jsob;
    }
}
