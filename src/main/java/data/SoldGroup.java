package data;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import net.sf.json.JSONObject;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SoldGroup {
    private final SimpleStringProperty id;
    private final SimpleStringProperty name;
    private final SimpleStringProperty records;
    private final SimpleStringProperty revenue;
    private final SimpleStringProperty tag;
    private final SimpleStringProperty keys;
    private static Map<String, SoldGroup> groups = new HashMap<>();

    public SoldGroup(String id, String name, String records, String revenue, String tag, String keys){
        this.id = new SimpleStringProperty(id);
        this.name = new SimpleStringProperty(name);
        this.records = new SimpleStringProperty(records);
        this.revenue = new SimpleStringProperty(revenue);
        this.tag = new SimpleStringProperty(tag);
        this.keys = new SimpleStringProperty(keys);
    }

    public static ObservableList<SoldGroup> toItem(){
        groups.clear();
        JSONObject soldStocks = DataSystem.loadStock(DataSystem.DATA_HISTORY_STOCK);
        for(Object key : soldStocks.keySet()){
            SoldStock soldStock = SoldStock.fromJson(soldStocks.getJSONObject(key.toString()));
            if(!groups.containsKey(soldStock.getId())){
                groups.put(soldStock.getId(), SoldGroup.newGroup(soldStock));
            }else{
                SoldGroup group = groups.get(soldStock.getId())
                        .updateRecord()
                        .updateRevenue(soldStock.getRevenue())
                        .updateKeys(soldStock.getKey());
                groups.put(soldStock.getId(), group);
            }
        }
        return FXCollections.observableArrayList(groups.values());
    }

    public static SoldGroup newGroup(SoldStock soldStocks){
        return new SoldGroup(soldStocks.getId(), soldStocks.getName(), "1", soldStocks.getRevenue(), "none", soldStocks.getKey());
    }

    public SoldGroup updateKeys(String key){
        setKeys(getKeys() + "," + key);
        return this;
    }

    public SoldGroup updateRevenue(String revenue){
        BigDecimal origin = new BigDecimal(getRevenue());
        BigDecimal bd_revenue = new BigDecimal(revenue);
        setRevenue(origin.add(bd_revenue).toPlainString());
        return this;
    }

    public SoldGroup updateRecord(){
        return updateRecord("1");
    }

    public SoldGroup updateRecord(String record){
        int origin = Integer.parseInt(getRecords());
        origin += Integer.parseInt(record);
        setRecords(String.valueOf(origin));
        return this;
    }

    public void removeGroup(){
        String[] keys = groups.get(getId()).getKeys().split(",");
        DataSystem.removeSoldGroup(keys);
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public SoldGroup setId(String id) {
        this.id.set(id);
        return this;
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public SoldGroup setName(String name) {
        this.name.set(name);
        return this;
    }

    public String getRecords() {
        return records.get();
    }

    public SimpleStringProperty recordsProperty() {
        return records;
    }

    public SoldGroup setRecords(String records) {
        this.records.set(records);
        return this;
    }

    public String getRevenue() {
        return revenue.get();
    }

    public SimpleStringProperty revenueProperty() {
        return revenue;
    }

    public SoldGroup setRevenue(String revenue) {
        this.revenue.set(revenue);
        return this;
    }

    public String getTag() {
        return tag.get();
    }

    public SimpleStringProperty tagProperty() {
        return tag;
    }

    public SoldGroup setTag(String tag) {
        this.tag.set(tag);
        return this;
    }

    public String getKeys() {
        return keys.get();
    }

    public SimpleStringProperty keysProperty() {
        return keys;
    }

    public SoldGroup setKeys(String keys) {
        this.keys.set(keys);
        return this;
    }
}
