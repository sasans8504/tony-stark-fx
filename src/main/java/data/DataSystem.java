package data;

import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.*;
import java.util.regex.Pattern;

import bin.Formula;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import net.sf.json.JSONObject;
import panes.HomePane;

public enum DataSystem {
    INSTANCE;
    public static final String DATA_FILE_PATH = "user_data.txt";
    public static final String SYS_FONT = "SansSerif";
    public static final String DATA_KEY = "key";
    public static final String DATA_ID = "id";
    public static final String DATA_NAME = "name";
    public static final String DATA_ALARM = "alarm";
    public static final String DATA_ALARM_LOW = "alarm_low";
    public static final String DATA_ALARM_HIGH = "alarm_high";
    public static final String DATA_PRICE_YESTERDAY = "price_ysclose";
    public static final String DATA_PRICE_OPEN = "price_open";
    public static final String DATA_PRICE_NOW = "price_now";
    public static final String DATA_PRICE_BUY = "price_buy";
    public static final String DATA_PRICE_SOLD = "price_sold";
    public static final String DATA_PRICE_MARKET = "price_market";
    public static final String DATA_COUNT = "count";
    public static final String DATA_FEE = "fee";
    public static final String DATA_BUY_FEE = "buy_fee";
    public static final String DATA_SOLD_FEE = "sold_fee";
    public static final String DATA_RECORDS = "records";
    public static final String DATA_REVENUE = "revenue";
    public static final String DATA_TIME_SOLD = "sold_time";
    public static final String DATA_TAG = "tag";
    public static final String DATA_TAX = "tax";
    public static final String DATA_TRACING_STOCK = "tracing";
    public static final String DATA_HISTORY_STOCK = "history";
    public static final String DATA_SYSTEM = "system";
    public static final String DATA_SYSTEM_LANGUAGE = "language";
    public static final String DATA_SYSTEM_F_BROKER_DISCOUNT = "broker_discount";
    public static final String DATA_SYSTEM_API = "api_info";
    public static final String DATA_API_TWSE = "twse";
    public static final String DATA_API_ANUE = "anue";
    public static final String DATA_LANGUAGE_TW = "zh-TW";
    public static final String DATA_LANGUAGE_US = "en-US";
    public static final String API_STOCK_YESTERDAY = "_ysclose";
    public static final String API_STOCK_OPEN = "_open";
    public static final String API_STOCK_NAME = "_name";
    public static final String API_STOCK_AMPLITUDE = "_amp";
    public static final String API_STATUS_FAIL = "fail";
    public static final String API_STATUS_EMPTY = " is empty";
    public static final String API_STATUS_ERROR = "#error ";
    public static String currentUser = "arthur";
    static JSONObject sysProperties;
    static JSONObject allData;
    static JSONObject userData;
    static ObservableList<Stock> stockList;
    static Map<String, Stock> stockMap = new HashMap<>();

    public void init() {
        File file = new File(DATA_FILE_PATH);
        StringBuffer sb = new StringBuffer();
        String dataStr;
        if (file.exists()) {
            try {
                InputStreamReader in = new InputStreamReader(new FileInputStream(DATA_FILE_PATH), StandardCharsets.UTF_8);
                int str;
                while ((str = in.read()) != -1) {
                    sb.append((char) str);
                }
                System.out.println("file data:" + sb);
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            dataStr = sb.toString();
            if ("".equals(dataStr)) dataStr = "{}";
        } else {
            dataStr = "{}";
        }
        allData = JSONObject.fromObject(dataStr);
        if (allData.has(DATA_SYSTEM)) {
            sysProperties = allData.getJSONObject(DATA_SYSTEM);
            if (sysProperties.has(DATA_SYSTEM_F_BROKER_DISCOUNT)) {
                Formula.setBroker_discount(sysProperties.getString(DATA_SYSTEM_F_BROKER_DISCOUNT));
            }
            allData.remove(DATA_SYSTEM);
        } else {
            sysProperties = new JSONObject();
            sysProperties.put(DATA_SYSTEM_LANGUAGE, DATA_LANGUAGE_TW);
            if (!sysProperties.has(DATA_SYSTEM_API)) sysProperties.put(DATA_SYSTEM_API, DATA_API_TWSE);
        }
    }

    private static void writeAllData() {
        try {
            allData.put(DATA_SYSTEM, sysProperties);
            System.out.println("allData:" + allData);
            System.out.println("file writing");
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(DATA_FILE_PATH), StandardCharsets.UTF_8);
            out.write(allData.toString());
            out.close();
            System.out.println("write complete");
            allData.remove(DATA_SYSTEM);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static Set getAllUser() {
        return allData.keySet();
    }

    public static void loadUser(String user) {
        userData = getUserInfo(user);
    }

    private static JSONObject getUserInfo(String user) {
        if (!allData.has(user)) {
            allData.put(user, new JSONObject());
            writeAllData();
        }
        currentUser = user;
        return allData.getJSONObject(user);
    }

    public static JSONObject loadStock() {
        return loadStock(DATA_TRACING_STOCK);
    }

    public static JSONObject loadStock(String type) {
        if (type.equals(DATA_HISTORY_STOCK)) {
            if (!userData.has(DATA_HISTORY_STOCK)) {
                userData.put(DATA_HISTORY_STOCK, new JSONObject());
            }
            return userData.getJSONObject(DATA_HISTORY_STOCK);
        } else {
            if (userData.isEmpty()) {
                userData.put(DATA_TRACING_STOCK, new JSONObject());
            }
            return userData.getJSONObject(DATA_TRACING_STOCK);
        }
    }

    public static String getSystemSetting(String key) {
        if (!sysProperties.has(key)) return API_STATUS_FAIL + ":no such system properties";
        else return sysProperties.getString(key);
    }

    public static void setSystemSetting(String key, String info) {
        boolean save = true;
        if (key.equals(DATA_SYSTEM_LANGUAGE)) {
            sysProperties.put(DATA_SYSTEM_LANGUAGE, info);
        } else if (key.equals(DATA_SYSTEM_F_BROKER_DISCOUNT)) {
            Formula.setBroker_discount(info);
            sysProperties.put(DATA_SYSTEM_F_BROKER_DISCOUNT, info);
        } else {
            System.out.println(API_STATUS_FAIL + ":" + key + " is not the correct system properties.");
            save = false;
        }

        if (save)
            writeAllData();
    }

    public static void stockAsBean() {
        stockList = FXCollections.observableArrayList();
        JSONObject stocks = loadStock(DATA_TRACING_STOCK);
        for (Object id : stocks.keySet()) {
            JSONObject stockObject = stocks.getJSONObject(id.toString());
            Stock stock = new Stock(stockObject);
            stockList.add(stock);
            stockMap.put(id.toString(), stock);
        }
    }

    public static ObservableList<Stock> getStockList() {
        return stockList;
    }

    public static void refreshStockList() {
        refreshStockList(DATA_TRACING_STOCK);
    }

    public static void refreshStockList(String type) {
        JSONObject stocks = loadStock(type);
        for (Object id : stocks.keySet()) {
            JSONObject stockObject = stocks.getJSONObject(id.toString());
            stockMap.get(id.toString()).setPrice_now(stockObject.getString(DATA_PRICE_NOW));
            stockMap.get(id.toString()).setPrice_ysclose(stockObject.getString(DATA_PRICE_YESTERDAY));
            stockMap.get(id.toString()).setPrice_open(stockObject.getString(DATA_PRICE_OPEN));
        }
        HomePane.resetColor();
    }

    public static void debugStockList(String price) {
        JSONObject stocks = loadStock();
        for (Object id : stocks.keySet()) {
            stockMap.get(id.toString()).setPrice_now(price);
        }
    }

    public static Stock getStockById(String id) {
        return stockMap.get(id);
    }

    public static void saveUserData2All() {
        allData.put(currentUser, userData);
        writeAllData();
    }

    public static void updateStockProperty(String type, Stock stock) {
        userData.getJSONObject(type).put(stock.getId(), stock.toString());
        saveUserData2All();
    }

    public static void addStock(String id, JSONObject stock) {
        userData.getJSONObject(DATA_TRACING_STOCK).put(id, stock);
        allData.put(currentUser, userData);
        writeAllData();
    }

    public static void sellStock(SoldStock stock){
        userData.getJSONObject(DATA_HISTORY_STOCK).put(stock.getKey(), stock.toJson());
        allData.put(currentUser, userData);
        writeAllData();
    }

    public static void removeStock(String type, String id) {
        userData.getJSONObject(type).remove(id);
        allData.put(currentUser, userData);
        writeAllData();
    }

    public static void removeSoldGroup(String... keys){
        for(String key : keys) {
            userData.getJSONObject(DATA_HISTORY_STOCK).remove(key);
        }
        allData.put(currentUser, userData);
        writeAllData();
    }

    public static void setLowAlarm(String id, String amount) {
        userData.getJSONObject(DATA_TRACING_STOCK).getJSONObject(id).put(DATA_ALARM_LOW, amount);
        saveUserData2All();
    }

    public static void setHighAlarm(String id, String amount) {
        userData.getJSONObject(DATA_TRACING_STOCK).getJSONObject(id).put(DATA_ALARM_HIGH, amount);
        saveUserData2All();
    }

    public static boolean isNumber(String value) {
        return Pattern.compile("^([-+]?\\d+)(\\.\\d+)?$").matcher(value).matches();
    }

    public static boolean isNumber(String... values){
        Pattern pattern = Pattern.compile("^([-+]?\\d+)(\\.\\d+)?$");
        for(String value: values){
            if(!pattern.matcher(value).matches()) return false;
        }
        return true;
    }

    public static boolean isNoSignNumber(String value){
        return Pattern.compile("^(\\d+)(\\.\\d+)?$").matcher(value).matches();
    }

    public static boolean isNumberOrAlpha(String value){
        return Pattern.compile("^[A-Za-z0-9]+$").matcher(value).matches();
    }

    public static int isIntBigger(String value, String compare){
        return Integer.compare(Integer.parseInt(value), Integer.parseInt(compare));
    }

    public static boolean isLegal(String id, String type, String value) {
        if (type.equals(DATA_ALARM_LOW)) {
            BigDecimal now = new BigDecimal(userData.getJSONObject(DATA_TRACING_STOCK).getJSONObject(id).getString(DATA_PRICE_NOW));
            BigDecimal low = new BigDecimal(value);
            int ans = now.compareTo(low);
            if (ans > 0) return true;
            else if (ans < 0) return false;
        } else if (type.equals(DATA_ALARM_HIGH)) {
            BigDecimal now = new BigDecimal(userData.getJSONObject(DATA_TRACING_STOCK).getJSONObject(id).getString(DATA_PRICE_NOW));
            BigDecimal high = new BigDecimal(value);
            int ans = now.compareTo(high);
            if (ans > 0) return false;
            else if (ans < 0) return true;
        }
        return false;
    }

    public static void setAlarmTo0(String id, String type) {
        String value = userData.getJSONObject(DATA_TRACING_STOCK).getJSONObject(id).getString(type);
        if (!"0".equals(value)) {
            userData.getJSONObject(DATA_TRACING_STOCK).getJSONObject(id).put(type, "0");
            saveUserData2All();
        }
    }

    public static void setAlarm(String id, boolean alarm, String... who) {
        if (who.length > 0) System.out.println("set by:" + who[0]);
        userData.getJSONObject(DATA_TRACING_STOCK).getJSONObject(id).put(DATA_ALARM, alarm);
        saveUserData2All();
    }
}
