package stage;

import bin.SFrameFX;
import bin.StockSchedule;
import bin.TonyStark;
import data.DataSystem;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.stage.Window;
import panes.HomePane;

public class DebugStage extends Stage {
    public DebugStage(){
        this(null);
    }

    public DebugStage(Window owner){
        BorderPane pane = new BorderPane();
        VBox center = new VBox();
        Button showUserData = new Button("show user data (json)");
        showUserData.setOnAction(e -> System.out.println());
        center.getChildren().add(showUserData);

        Button showUserStocks = new Button("show user stocks (json)");
        showUserStocks.setOnAction(e -> System.out.println(DataSystem.loadStock()));
        center.getChildren().add(showUserStocks);

        Button historyData = new Button("show user history data");
        historyData.setOnAction(e -> System.out.println(DataSystem.loadStock(DataSystem.DATA_HISTORY_STOCK)));
        center.getChildren().add(historyData);

        Button saveData = new Button("save user data");
        saveData.setOnAction(e -> DataSystem.saveUserData2All());
        center.getChildren().add(saveData);

        Button refreshDetailPage100 = new Button("modify stock info (100)");
        refreshDetailPage100.setOnAction(e -> {
            DataSystem.debugStockList("100");
            System.out.println("refresh success");
        });
        center.getChildren().add(refreshDetailPage100);

        Button refreshDetailPage0 = new Button("modify stock info (5)");
        refreshDetailPage0.setOnAction(e -> {
            DataSystem.debugStockList("100");
            System.out.println("refresh success");
        });
        center.getChildren().add(refreshDetailPage0);

        Button stageCount = new Button("Stage count");
        stageCount.setOnAction(e -> {
            System.out.println("other stage count:"+SFrameFX.countStages());
        });
        center.getChildren().add(stageCount);

        Button switchNetworking = new Button("network: " + SFrameFX.isNetworking());
        switchNetworking.setOnAction(e -> {
            SFrameFX.switchNetworking();
            switchNetworking.setText("network: "+ SFrameFX.isNetworking());
        });
        center.getChildren().add(switchNetworking);

        Button switchForceRun = new Button("query once (networking needed)");
        switchForceRun.setOnAction(e -> SFrameFX.switchForceRun());
        center.getChildren().add(switchForceRun);

        Button forceQuery = new Button("force query once (networking needed)");
        forceQuery.setOnAction(e -> new Thread(new StockSchedule()));
        center.getChildren().add(forceQuery);

        Label top = new Label("---- Developer tools ----");
        top.setFont(Font.font("Sanserif", FontWeight.BOLD, FontPosture.ITALIC, 40));
        pane.setTop(top);
        pane.setCenter(center);

        SFrameFX.registeredStage(this);
        setTitle("Developer tools");
        setScene(new Scene(pane));
        setResizable(false);
        initOwner(owner);
        setOnCloseRequest(event -> SFrameFX.removeStage(this));
        show();
    }


}
