package stage;

import bin.Formula;
import bin.SFrameFX;
import bin.TonyStark;
import data.SoldStock;
import data.Stock;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.stage.Window;
import panes.TonyPane;

import static data.DataSystem.*;


public class SellStage extends Stage {
    public SellStage(TonyPane parentPane, Stock stock) {
        this(null, parentPane, stock);
    }

    public SellStage(Window owner, TonyPane parentPane, Stock stock) {
        GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);
        pane.setHgap(10);
        pane.setVgap(10);
        pane.add(new Label(stock.getId() + " " + stock.getName()), 0, 0, 2, 1);
        pane.add(new Label(TonyStark.getBundle("sell.stage.own.count")), 0, 1);
        pane.add(new Label(stock.getCount()), 1, 1);
        pane.add(new Label(TonyStark.getBundle("sell.stage.sell.price")), 0, 2);
        TextField sellPrice = new TextField();
        pane.add(sellPrice, 1, 2);
        sellPrice.setText(stock.getPrice_buy());
        sellPrice.setEditable(false);
        pane.add(new Label(TonyStark.getBundle("sell.stage.sell.count")), 0, 3);
        TextField count = new TextField();
        pane.add(count, 1, 3);
        Button sell = new Button(TonyStark.getBundle("sell.stage.sell"));
        sell.setFont(Font.font(SYS_FONT, FontWeight.BOLD, FontPosture.ITALIC, 18));
        sell.setOnAction(e -> {
            if (isNumber(count.getText())) {
                SoldStock soldStock = SoldStock.fromStock(stock);
                if (isIntBigger(count.getText(), stock.getCount()) > 0) {
                    count.setText(stock.getCount());
                }
                String buyFee = Formula.splitFee(stock, count.getText());
                String soldFee = Formula.fee(sellPrice.getText(), count.getText());
                String tax = Formula.tax(stock, count.getText());
                soldStock.sellInit(stock.getPrice_buy(), count.getText(), buyFee, soldFee, tax);
                ButtonType confirm = SFrameFX.alertMsgAndWait(
                        TonyStark.getBundle("data.check.notice"),
                        TonyStark.getBundle("detail.page.fee.buy") + " : " + buyFee
                                + "\n" + TonyStark.getBundle("sell.stage.sell.fee") + " : " + soldFee
                                + "\n" + TonyStark.getBundle("sell.stage.tax") + " : " + tax
                                + "\n" + TonyStark.getBundle("sell.stage.revenue") + " : " + soldStock.getRevenue());
                if (confirm == ButtonType.OK) {
                    stock.minusCount(count.getText());
                    stock.updateBuyFee(buyFee);
                    updateStockProperty(DATA_TRACING_STOCK, stock);
                    sellStock(soldStock);
                    SFrameFX.alertMsg(TonyStark.getBundle("data.update"), TonyStark.getBundle("data.success"));
                    close();
                }

            } else {
                SFrameFX.alertMsg(TonyStark.getBundle("data.error"), TonyStark.getBundle("data.error.number.illegal"));
            }
        });
        pane.add(sell, 1, 4);

        SFrameFX.registeredStage(this);
        setTitle(TonyStark.getBundle("sell.stage.sell"));
        setScene(new Scene(pane, 250, 200));
        initOwner(owner);
        setResizable(false);
        setOnCloseRequest(event -> SFrameFX.removeStage(this));
        show();
    }
}
