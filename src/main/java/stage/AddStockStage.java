package stage;

import bin.AnueAPI;
import bin.Formula;
import bin.SFrameFX;
import bin.TonyStark;
import data.DataSystem;
import data.Stock;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.stage.Window;
import net.sf.json.JSONObject;
import panes.TonyPane;

import java.util.Map;

import static data.DataSystem.*;

/**
 * 1.alert may show if count is not 0 but price is 0.
 * 2.if all info not empty, isNumber() will do the check.
 * 3.loadStock() run first to check id, if stock not match in user's tracing stock,
 * then call api to get stock info.
 */
public class AddStockStage extends Stage {
    public AddStockStage(TonyPane parentPane) {
        this(null, parentPane);
    }

    public AddStockStage(Window owner, TonyPane parentPane) {
        GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);
        pane.setHgap(10);
        pane.setVgap(10);
        pane.add(new Label(TonyStark.getBundle("detail.page.id")), 0, 0);
        TextField id = new TextField();
        id.setPrefColumnCount(5);
        pane.add(id, 1, 0);
        pane.add(new Label(TonyStark.getBundle("detail.page.price.buy")), 0, 1);
        TextField buyPrice = new TextField();
        pane.add(buyPrice, 1, 1);
        pane.add(new Label(TonyStark.getBundle("detail.page.count")), 0, 2);
        TextField count = new TextField();
        pane.add(count, 1, 2);
        Button addStock = new Button(TonyStark.getBundle("detail.page.add"));
        addStock.setFont(Font.font(SYS_FONT, FontWeight.BOLD, FontPosture.ITALIC, 18));
        addStock.setOnAction(e -> {
            if (id.getText().trim().isEmpty()
                    || count.getText().trim().isEmpty()
                    || buyPrice.getText().trim().isEmpty()) {
                SFrameFX.alertMsg(TonyStark.getBundle("add.stage.empty"), TonyStark.getBundle("add.stage.empty.confirm"));
            } else if (!count.getText().equals("0") && buyPrice.getText().equals("0")) {
                SFrameFX.alertMsg(TonyStark.getBundle("data.error"), TonyStark.getBundle("data.error.zero.stock"));
            } else {
                if (isNumber(id.getText(), buyPrice.getText(), count.getText())) {
                    if (loadStock().has(id.getText())) {
                        Map<String, String> result = Formula.merge(getStockById(id.getText()), buyPrice.getText(), count.getText());
                        ButtonType confirm = SFrameFX.alertMsgAndWait(
                                TonyStark.getBundle("data.check.notice"),
                                TonyStark.getBundle("add.stage.after.merge")
                                        + "\n" + TonyStark.getBundle("detail.page.price.buy") + " : " + result.get(Formula.NEW_PRICE)
                                        + "\n" + TonyStark.getBundle("detail.page.count") + " : " + result.get(Formula.NEW_COUNT)
                                        + "\n(" + TonyStark.getBundle("add.stage.total") + ")"
                                        + TonyStark.getBundle("detail.page.fee.buy") + " : " + result.get(Formula.NEW_FEE));
                        if (confirm == ButtonType.OK) {
                            Stock stock = getStockById(id.getText())
                                    .setPrice_buy(result.get(Formula.NEW_PRICE))
                                    .setCount(result.get(Formula.NEW_COUNT))
                                    .setBuy_fee(result.get(Formula.NEW_FEE));
                            updateStockProperty(DATA_TRACING_STOCK, stock);
                            SFrameFX.alertMsg(TonyStark.getBundle("data.update"), TonyStark.getBundle("data.success"));
                            close();
                        }
                    } else {
                        String stockData = AnueAPI.getMultiStocks(id.getText());
                        if (AnueAPI.errorCheck(stockData)) {
                            SFrameFX.alertMsg(TonyStark.getBundle("add.stage.query.fail"), stockData);
                        } else {
                            JSONObject stockObj = JSONObject.fromObject(stockData);
                            JSONObject stock = new JSONObject();
                            stock.put(DATA_ID, id.getText());
                            stock.put(DATA_NAME, stockObj.getString(id.getText() + API_STOCK_NAME));
                            stock.put(DATA_ALARM, false);
                            stock.put(DATA_ALARM_LOW, 0);
                            stock.put(DATA_ALARM_HIGH, 0);
                            stock.put(DATA_PRICE_YESTERDAY, stockObj.getString(id.getText() + API_STOCK_YESTERDAY));
                            stock.put(DATA_PRICE_OPEN, stockObj.getString(id.getText() + API_STOCK_OPEN));
                            stock.put(DATA_PRICE_NOW, stockObj.getString(id.getText()));
                            stock.put(DATA_PRICE_BUY, buyPrice.getText());
                            stock.put(DATA_PRICE_SOLD, "0.00");
                            stock.put(DATA_COUNT, count.getText());
                            stock.put(DATA_BUY_FEE, Formula.fee(buyPrice.getText(), count.getText()));
                            DataSystem.addStock(id.getText(), stock);
                            SFrameFX.alertMsg(TonyStark.getBundle("detail.page.add"), TonyStark.getBundle("data.success"));
                            parentPane.matchData();
                            close();
                        }
                    }
                } else {
                    SFrameFX.alertMsg(TonyStark.getBundle("data.error"), TonyStark.getBundle("data.error.number.illegal"));
                }
            }
        });
        pane.add(addStock, 1, 4);

        SFrameFX.registeredStage(this);
        setTitle(TonyStark.getBundle("detail.page.add"));
        setScene(new Scene(pane, 250, 200));
        initOwner(owner);
        setResizable(false);
        setOnCloseRequest(event -> SFrameFX.removeStage(this));
        show();
    }
}
