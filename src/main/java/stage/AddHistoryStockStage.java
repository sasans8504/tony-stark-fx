package stage;

import bin.AnueAPI;
import bin.SFrameFX;
import data.DataSystem;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import net.sf.json.JSONObject;
import panes.HomePane;

import static data.DataSystem.*;


public class AddHistoryStockStage extends Stage {
    public AddHistoryStockStage() {
        GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);
        pane.setHgap(10);
        pane.setVgap(10);
        pane.add(new Label("股票代碼"), 0, 0);
        TextField id = new TextField();
        id.setPrefColumnCount(5);
        pane.add(id, 1, 0);
        pane.add(new Label("股票名稱"), 0, 1);
        TextField name = new TextField();
        pane.add(name, 1, 1);
        pane.add(new Label("數量"), 0, 2);
        TextField count = new TextField();
        pane.add(count, 1, 2);
        pane.add(new Label("賣出價格"), 0, 3);
        TextField buyPrice = new TextField();
        pane.add(buyPrice, 1, 3);
        Button addStock = new Button("新增");
        addStock.setFont(Font.font(SYS_FONT, FontWeight.BOLD, FontPosture.ITALIC, 18));
        addStock.setOnAction(e -> {
            if (id.getText().trim().isEmpty()
                    || name.getText().trim().isEmpty()
                    || count.getText().trim().isEmpty()
                    || buyPrice.getText().trim().isEmpty()) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("尚有欄位為空");
                alert.setContentText("請檢查未填欄位");
                alert.show();
            } else {
                if(loadStock().has(id.getText())){
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("資料庫中已經存在");
                    alert.setContentText("正在追蹤的股票中已經有該隻股票，按下 確定 將重新計算目前的成本並存入。");
                    alert.showAndWait();
                }else {
                    String nowPrice = AnueAPI.getMultiStocks(id.getText());
                    if (AnueAPI.errorCheck(nowPrice)) {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("無法查詢到此股票");
                        alert.setContentText(nowPrice);
                        alert.show();
                    } else {
                        JSONObject stock = new JSONObject();
                        stock.put(DATA_NAME, name.getText());
                        stock.put(DATA_ALARM, false);
                        stock.put(DATA_ALARM_LOW, 0);
                        stock.put(DATA_ALARM_HIGH, 0);
                        stock.put(DATA_PRICE_NOW, JSONObject.fromObject(nowPrice).getString(id.getText()));
                        stock.put(DATA_PRICE_BUY, buyPrice.getText());
                        stock.put(DATA_PRICE_SOLD, "0.00");
                        stock.put(DATA_COUNT, count.getText());
                        DataSystem.addStock(id.getText(), stock);
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("新增成功");
                        alert.setContentText("新增成功");
                        alert.show();
                        HomePane.getInstance();
                        close();
                    }
                }
            }
        });
        pane.add(addStock, 1, 4);

        SFrameFX.registeredStage(this);
        setTitle("新增股票");
        setScene(new Scene(pane, 250, 200));
        setResizable(false);
        setOnCloseRequest(event -> SFrameFX.removeStage(this));
        show();
    }
}
