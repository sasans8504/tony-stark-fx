package panes;

import bin.SFrameFX;
import bin.TonyStark;
import data.DataSystem;
import data.SoldGroup;
import data.SoldStock;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import java.time.LocalDate;

import static data.DataSystem.*;

public class HistoryPane extends VBox implements TonyPane {
    final String THIS_PANE = DATA_HISTORY_STOCK;
    private static HistoryPane instance;
    private TableView<SoldGroup> topTable;
    private TableView<SoldStock> belowTable;

    public static HistoryPane getInstance() {
        if (instance == null) {
            instance = new HistoryPane();
        }
        instance.matchData();
        return instance;
    }

    public static void dispose() {
        if (instance != null) instance = null;
    }

    HistoryPane() {
        setStyle("-fx-background-color: white");
        start();
    }

    private void start() {
        topTable = new TableView<>();

        TableColumn<SoldGroup, String> id = toGroupColumn(TonyStark.getBundle("detail.page.id"), 70, DATA_ID);
        TableColumn<SoldGroup, String> name = toGroupColumn(TonyStark.getBundle("detail.page.name"), 100, DATA_NAME);
        TableColumn<SoldGroup, String> records = toGroupColumn(TonyStark.getBundle("history.page.records"), 50, DATA_RECORDS);
        TableColumn<SoldGroup, String> revenues = toGroupColumn(TonyStark.getBundle("sell.stage.revenue"), 70, DATA_REVENUE);
        TableColumn<SoldGroup, String> tag = toGroupColumn(TonyStark.getBundle("history.page.tag"), 70, DATA_TAG);
        TableColumn<SoldGroup, Void> deleteRow = toColumnButton();

        topTable.getColumns().addAll(id, name, records, revenues, tag, deleteRow);

        belowTable = new TableView<>();
        TableColumn<SoldStock, String> marketPrice = toSoldColumn(TonyStark.getBundle("history.page.market.price"), 70, DATA_PRICE_MARKET);
        TableColumn<SoldStock, String> soldPrice = toSoldColumn(TonyStark.getBundle("history.page.sold.price"), 70, DATA_PRICE_SOLD);
        TableColumn<SoldStock, String> count = toSoldColumn(TonyStark.getBundle("detail.page.count"), 60, DATA_COUNT);
        TableColumn<SoldStock, String> soldTime = toSoldColumn(TonyStark.getBundle("history.page.sold.date"), 70, DATA_TIME_SOLD);
        TableColumn<SoldStock, String> fee = toSoldColumn(TonyStark.getBundle("history.page.fee"), 70, DATA_FEE);
        TableColumn<SoldStock, String> buyFee = toSoldColumn(TonyStark.getBundle("history.page.buy"), 70, DATA_BUY_FEE);
        TableColumn<SoldStock, String> soldFee = toSoldColumn(TonyStark.getBundle("history.page.sold"), 70, DATA_SOLD_FEE);
        fee.getColumns().addAll(buyFee, soldFee);
        TableColumn<SoldStock, String> tax = toSoldColumn(TonyStark.getBundle("sell.stage.tax"), 70, DATA_TAX);
        TableColumn<SoldStock, String> revenue = toSoldColumn(TonyStark.getBundle("sell.stage.revenue"), 70, DATA_REVENUE);

        belowTable.getColumns().addAll(marketPrice, soldPrice, count, soldTime, fee, tax, revenue);

        BorderPane middle = new BorderPane();
        HBox middleLeft = new HBox(20);
        middleLeft.setAlignment(Pos.CENTER_LEFT);
        middleLeft.setPadding(new Insets(0, 0, 0, 20));
        HBox middleRight = new HBox(10);
        middleRight.setAlignment(Pos.CENTER_RIGHT);
        Label idText = new Label();
        idText.setText("");
        Label nameText = new Label();
        nameText.setText("");
        middleLeft.getChildren().addAll(idText, nameText);
        Label fromText = new Label();
        fromText.setText(TonyStark.getBundle("history.page.from"));
        DatePicker from = new DatePicker();
        from.setValue(LocalDate.now());
        Label toText = new Label();
        toText.setText(TonyStark.getBundle("history.page.to"));
        DatePicker to = new DatePicker();
        middleRight.getChildren().addAll(fromText, from, toText, to);
        middle.setLeft(middleLeft);
        middle.setRight(middleRight);
        EventHandler<ActionEvent> timeCheckEvent = event -> {
            if (!"".equals(idText.getText())) {
                if (from.getValue() != null && to.getValue() != null) {
                    belowTable.setItems(SoldStock.toItem(idText.getText(), from.getValue(), to.getValue()));
                } else if (from.getValue() != null) {
                    belowTable.setItems(SoldStock.toItemFrom(idText.getText(), from.getValue()));
                } else if (to.getValue() != null) {
                    belowTable.setItems(SoldStock.toItemTo(idText.getText(), to.getValue()));
                } else {
                    belowTable.setItems(SoldStock.toItem(idText.getText()));
                }
            }
        };
        from.setOnAction(timeCheckEvent);
        to.setOnAction(timeCheckEvent);

        BorderPane below = new BorderPane();
        HBox functions = new HBox(10);
        functions.setAlignment(Pos.CENTER_LEFT);
        HBox belowRight = new HBox(10);
        belowRight.setAlignment(Pos.CENTER_RIGHT);
        Button delete = new Button(TonyStark.getBundle("detail.page.function.delete"));
        delete.setOnAction(e -> {
            if (selectNotNull(belowTable)) {
                ButtonType confirm = SFrameFX.alertMsgAndWait(
                        TonyStark.getBundle("detail.page.function.delete"),
                        TonyStark.getBundle("data.delete.confirm"));
                if (confirm == ButtonType.OK) {
                    DataSystem.removeStock(THIS_PANE, belowTable.getSelectionModel().getSelectedItem().getKey());
                    matchData();
                }
            }
        });
        functions.getChildren().add(delete);
        Button output = new Button(TonyStark.getBundle("history.page.output"));
        output.setDisable(true);
        output.setOnAction(e -> {
        });
        belowRight.getChildren().add(output);
        below.setLeft(functions);
        below.setRight(belowRight);

        topTable.getSelectionModel().selectedItemProperty().addListener(((observable) -> {
            if (selectNotNull(topTable, false)) {
                idText.setText(topTable.getSelectionModel().getSelectedItem().getId());
                nameText.setText(topTable.getSelectionModel().getSelectedItem().getName());
                belowTable.setItems(SoldStock.toItem(topTable.getSelectionModel().getSelectedItem().getId()));
            } else {
                idText.setText("");
                nameText.setText("");
                belowTable.setItems(null);
            }
        }));

        getChildren().addAll(topTable, middle, belowTable, below);
    }

    private TableColumn<SoldGroup, String> toGroupColumn(String name, int minWidth, String type) {
        TableColumn<SoldGroup, String> column = new TableColumn<>(name);
        column.setId(type);
        column.setMinWidth(minWidth);
        column.setCellValueFactory(
                new PropertyValueFactory<>(type));
        column.setCellFactory(TextFieldTableCell.forTableColumn());
        return column;
    }

    private TableColumn<SoldStock, String> toSoldColumn(String name, int minWidth, String type) {
        TableColumn<SoldStock, String> column = new TableColumn<>(name);
        column.setId(type);
        column.setMinWidth(minWidth);
        column.setCellValueFactory(
                new PropertyValueFactory<>(type));
        column.setCellFactory(TextFieldTableCell.forTableColumn());
        return column;
    }

    private TableColumn<SoldGroup, Void> toColumnButton() {
        TableColumn<SoldGroup, Void> column = new TableColumn<>(TonyStark.getBundle("detail.page.function"));
        column.setMinWidth(50);
        Callback<TableColumn<SoldGroup, Void>, TableCell<SoldGroup, Void>> cellFactory = new Callback<TableColumn<SoldGroup, Void>, TableCell<SoldGroup, Void>>() {
            @Override
            public TableCell<SoldGroup, Void> call(TableColumn<SoldGroup, Void> param) {
                return new TableCell<SoldGroup, Void>() {
                    private final Button delete = new Button(TonyStark.getBundle("detail.page.function.delete"));

                    {
                        delete.setOnAction(event -> {
                            ButtonType confirm = SFrameFX.alertMsgAndWait(
                                    TonyStark.getBundle("detail.page.function.delete"),
                                    TonyStark.getBundle("data.delete.confirm"));
                            if (confirm == ButtonType.OK) {
                                getTableView().getItems().get(getIndex()).removeGroup();
                            }
                            matchData();
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(delete);
                        }
                    }
                };
            }
        };

        column.setCellFactory(cellFactory);
        return column;
    }

    private boolean selectNotNull(TableView table) {
        return selectNotNull(table, true);
    }

    private boolean selectNotNull(TableView table, boolean alert) {
        if (table.getSelectionModel().getSelectedItem() == null && alert) {
            SFrameFX.alertMsg(TonyStark.getBundle("data.error"), TonyStark.getBundle("data.error.select.none"));
        }
        return table.getSelectionModel().getSelectedItem() != null;
    }

    public void matchData() {
        topTable.setItems(SoldGroup.toItem());
    }
}
