package panes;

import bin.Formula;
import bin.SFrameFX;
import bin.TonyStark;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import net.sf.json.JSONObject;
import stage.AddStockStage;

import java.util.HashMap;
import java.util.Map;

import static data.DataSystem.*;

public class HomePane extends FlowPane implements TonyPane {
    private static HomePane instance;
    private static Map<String, Pane> stockPanes = new HashMap<>();
    private static Map<String, HBox> alarms = new HashMap<>();
    private static Map<String, Label> stockColor = new HashMap<>();

    public static HomePane getInstance() {
        if (instance == null) {
            instance = new HomePane();
        } else {
            instance.matchData();
        }
        return instance;
    }

    public static void dispose() {
        if (instance != null) instance = null;
    }

    HomePane() {
        setStyle("-fx-background-color: white");
        start();
    }

    private void start() {
        stockAsBean();
        JSONObject stocks = loadStock();
        for (Object oid : stocks.keySet()) {
            String id = (String) oid;
            JSONObject stock = stocks.getJSONObject(id);
            stock.put(DATA_ID, id);
            getChildren().add(stockObject(stock));
        }
        getChildren().add(addButtonObject());
    }

    private VBox addButtonObject() {
        VBox pane = new VBox();
        pane.setPadding(new Insets(10, 10, 10, 10));
        pane.setStyle("-fx-border-color: black; -fx-background-color: silver");

        Button addStock = new Button("+");
        addStock.setFont(Font.font(SYS_FONT, FontWeight.BOLD, FontPosture.ITALIC, 50));
        addStock.setOnAction(e -> new AddStockStage(getScene().getWindow(), this));
        pane.getChildren().add(addStock);
        pane.getChildren().add(new Label(TonyStark.getBundle("home.page.add.stock")));
        stockPanes.put("plus", pane);

        return pane;
    }

    private VBox stockObject(JSONObject stocks) {
        VBox pane = new VBox();
        pane.setPadding(new Insets(10, 10, 10, 10));
        pane.setStyle("-fx-border-color: black; -fx-background-color: silver");
        HBox title = new HBox(5);
        String id = stocks.getString(DATA_ID);
        stockPanes.put(id, pane);

        title.getChildren().add(new Label(id));
        title.getChildren().add(new Label(stocks.getString(DATA_NAME)));

        HBox price = new HBox();
        price.setAlignment(Pos.CENTER);
        Label nowPrice = new Label(stocks.getString(DATA_PRICE_NOW));
        nowPrice.textProperty().bind(getStockById(id).price_nowProperty());
        nowPrice.setFont(Font.font(SYS_FONT, FontWeight.BOLD, FontPosture.ITALIC, 60));
        stockColor.put(id, nowPrice);
        setColor(id);
        price.getChildren().add(nowPrice);

        HBox alarm = new HBox();
        alarm.setAlignment(Pos.CENTER);
        TextField lowAlarm = new TextField();
        lowAlarm.setPromptText(TonyStark.getBundle("home.page.alarm.low"));
        lowAlarm.setPrefColumnCount(4);
        lowAlarm.setTooltip(new Tooltip(TonyStark.getBundle("home.page.alarm.hint")));
        if (!"0".equals(stocks.getString(DATA_ALARM_LOW)))
            lowAlarm.setText(stocks.getString(DATA_ALARM_LOW));
        lowAlarm.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                String low = lowAlarm.getText();
                if (!low.isEmpty()) {
                    if (isNumber(low) && isLegal(id, DATA_ALARM_LOW, low)) {
                        setLowAlarm(id, low);
                        SFrameFX.alertMsg(TonyStark.getBundle("data.success"), TonyStark.getBundle("data.success.save.msg"));
                        pane.requestFocus();
                    } else {
                        SFrameFX.alertMsg(TonyStark.getBundle("data.error"), TonyStark.getBundle("data.error.alarm.low.msg"));
                        lowAlarm.setText("");
                    }
                } else {
                    setAlarmTo0(id, DATA_ALARM_LOW);
                    pane.requestFocus();
                }
            }
        });
        alarm.getChildren().add(lowAlarm);
        CheckBox trigger = new CheckBox();
        trigger.setSelected(stocks.getBoolean(DATA_ALARM));
        trigger.setOnAction(event -> setAlarm(id, trigger.isSelected(), "HomePane"));
        alarm.getChildren().add(trigger);
        TextField highAlarm = new TextField();
        highAlarm.setPromptText(TonyStark.getBundle("home.page.alarm.high"));
        highAlarm.setPrefColumnCount(4);
        highAlarm.setTooltip(new Tooltip(TonyStark.getBundle("home.page.alarm.hint")));
        if (!"0".equals(stocks.getString(DATA_ALARM_HIGH)))
            highAlarm.setText(stocks.getString(DATA_ALARM_HIGH));
        highAlarm.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                String high = highAlarm.getText();
                if (!high.isEmpty()) {
                    if (isNumber(high) && isLegal(id, DATA_ALARM_HIGH, high)) {
                        setHighAlarm(id, high);
                        SFrameFX.alertMsg(TonyStark.getBundle("data.success"), TonyStark.getBundle("data.success.save.msg"));
                        pane.requestFocus();
                    } else {
                        SFrameFX.alertMsg(TonyStark.getBundle("data.error"), TonyStark.getBundle("data.error.alarm.high.msg"));
                        highAlarm.setText("");
                    }
                } else {
                    setAlarmTo0(id, DATA_ALARM_HIGH);
                    pane.requestFocus();
                }
            }
        });
        alarm.getChildren().add(highAlarm);
        alarms.put(id, alarm);

        HBox other = new HBox();
        other.setAlignment(Pos.CENTER);
        Button detail = new Button(TonyStark.getBundle("tray.detail.page"));
        detail.setFont(Font.font(SYS_FONT, FontWeight.BOLD, FontPosture.ITALIC, 20));
        detail.setOnAction(event -> SFrameFX.showStage(DetailPane.getInstance().select(getStockById(id))));
        other.getChildren().add(detail);

        pane.getChildren().addAll(title, price, alarm, other);

        return pane;
    }

    public static void alarmMsg(String title, String msg, String id) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setContentText(msg);
        alert.show();
        for (Node node : alarms.get(id).getChildren()) {
            if (node instanceof CheckBox) ((CheckBox) node).setSelected(false);
            if (node instanceof TextField) ((TextField) node).setText("");
        }
    }

    public void matchData() {
        getChildren().removeAll(getChildren());
        start();
    }

    private void setColor(String id) {
        stockColor.get(id).setTextFill(Formula.isRed(getStockById(id)));
    }

    public static void resetColor() {
        for (String id : stockColor.keySet()) {
            stockColor.get(id).setTextFill(Formula.isRed(getStockById(id)));
        }
    }

    public static void printAlarmStatus() {
        for (String key : alarms.keySet()) {
            for (Node node : alarms.get(key).getChildren()) {
                if (node instanceof CheckBox) System.out.println("node checkbox:" + ((CheckBox) node).isSelected());
                if (node instanceof TextField) System.out.println("node textfield:" + ((TextField) node).getText());
            }
        }
    }
}
