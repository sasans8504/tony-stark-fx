package panes;

import bin.AnueAPI;
import bin.Formula;
import bin.SFrameFX;
import bin.TonyStark;
import data.DataSystem;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;

import java.util.HashMap;
import java.util.Map;

import static bin.TonyStark.getBundle;
import static data.DataSystem.*;

public class SettingPane extends FlowPane implements TonyPane {
    private static SettingPane instance;
    private Map<String, Control> settingValues = new HashMap<>();
    public static SettingPane getInstance() {
        if (instance == null) {
            instance = new SettingPane();
        } else {
            instance.matchData();
        }
        return instance;
    }
    private String[] discountRange = new String[]
            {"100", "95", "90", "85", "80", "75",
                    "70", "65", "60", "55", "50",
                    "45", "40", "35", "30", "25",
                    "20", "15", "10"};

    SettingPane(){
        setAlignment(Pos.CENTER);
        GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);
        pane.setHgap(10);
        pane.setVgap(10);

        pane.add(new Label(getBundle("setting.page.broker.discount") + ":"), 0, 0);
        Label discountDesc = new Label(TonyStark.getBundle("setting.page.discount.desc"));
        discountDesc.setFont(Font.font(DataSystem.SYS_FONT, FontWeight.LIGHT, FontPosture.ITALIC, 12));
        pane.add(discountDesc, 0, 1, 3, 1);
        ObservableList<String> discounts = FXCollections.observableArrayList(discountRange);
        ComboBox<String> discount = new ComboBox<>(discounts);
        discount.setValue(Formula.getBroker_discount());
        settingValues.put(DATA_SYSTEM_F_BROKER_DISCOUNT, discount);
        pane.add(discount, 1, 0);
        pane.add(new Label(" (%)"), 2, 0);

        pane.add(new Label(getBundle("setting.page.handling.fee") + ":"), 0, 2);
        TextField feeRate = new TextField();
        feeRate.setEditable(false);
        feeRate.setText(Formula.getFee_rate());
        pane.add(feeRate, 1, 2);
        pane.add(new Label(" (%)"), 2, 2);

        pane.add(new Label(getBundle("setting.page.transfer.tax") + ":"), 0, 3);
        TextField taxRate = new TextField();
        taxRate.setEditable(false);
        taxRate.setText(Formula.getTransfer_tax_rate());
        pane.add(taxRate, 1, 3);
        pane.add(new Label(" (%)"), 2, 3);

        Button save = new Button(getBundle("setting.page.save"));
        save.setFont(Font.font(SYS_FONT, FontWeight.BOLD, FontPosture.ITALIC, 18));
        save.setOnAction(e -> {
            DataSystem.setSystemSetting(DATA_SYSTEM_F_BROKER_DISCOUNT, discount.getValue());
            SFrameFX.alertMsg(TonyStark.getBundle("setting.page.save"), TonyStark.getBundle("data.success"));
        });
        pane.add(save, 1, 4);
        getChildren().add(pane);
    }

    public static void dispose() {
        if (instance != null) instance = null;
    }

    @Override
    public void matchData() {
        for(String key : settingValues.keySet()){
            if(key.equals(DATA_SYSTEM_F_BROKER_DISCOUNT)) ((ComboBox) settingValues.get(key)).setValue(Formula.getBroker_discount());
        }
    }
}
