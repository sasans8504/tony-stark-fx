package panes;

import bin.SFrameFX;
import bin.TonyStark;
import data.DataSystem;
import data.Stock;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import stage.AddStockStage;
import stage.SellStage;

import static data.DataSystem.*;

public class DetailPane extends VBox implements TonyPane {
    final String THIS_PANE = DATA_TRACING_STOCK;
    private static DetailPane instance;
    private TableView<Stock> table;

    public static DetailPane getInstance() {
        if (instance == null) {
            instance = new DetailPane();
        }
        instance.matchData();
        return instance;
    }

    public DetailPane select(Stock stock) {
        table.getSelectionModel().select(stock);
        return instance;
    }

    public static void dispose() {
        if (instance != null) instance = null;
    }

    DetailPane() {
        setStyle("-fx-background-color: white");
        start();
    }

    private void start() {
        table = new TableView<>();

        table.setEditable(true);
        TableColumn<Stock, String> id = toColumn(TonyStark.getBundle("detail.page.id"), 70, DATA_ID);
        TableColumn<Stock, String> name = toColumn(TonyStark.getBundle("detail.page.name"), 100, DATA_NAME);
        TableColumn<Stock, String> ysPrice = toColumn(TonyStark.getBundle("detail.page.price.yesterday"), 60, DATA_PRICE_YESTERDAY);
        TableColumn<Stock, String> openPrice = toColumn(TonyStark.getBundle("detail.page.price.open"), 60, DATA_PRICE_OPEN);
        TableColumn<Stock, String> nowPrice = toColumn(TonyStark.getBundle("detail.page.price.now"), 60, DATA_PRICE_NOW);
        TableColumn<Stock, String> buyPrice = toColumn(TonyStark.getBundle("detail.page.price.buy"), 60, DATA_PRICE_BUY);
        TableColumn<Stock, String> count = toColumn(TonyStark.getBundle("detail.page.count"), 60, DATA_COUNT);
        TableColumn<Stock, String> buyFee = toColumn(TonyStark.getBundle("detail.page.fee.buy"), 60, DATA_BUY_FEE);

        table.getColumns().addAll(id, name, ysPrice, openPrice, nowPrice, buyPrice, count, buyFee);

        HBox hb = new HBox(10);
        hb.setAlignment(Pos.CENTER_LEFT);
        Button add = new Button(TonyStark.getBundle("detail.page.add"));
        add.setOnAction(e -> new AddStockStage(getScene().getWindow(), this));
        hb.getChildren().add(add);
        Button sell = new Button(TonyStark.getBundle("detail.page.function.sell"));
        sell.setOnAction(e -> {
            if (selectNotNull() && selectCountNotZero()) {
                new SellStage(getScene().getWindow(), this, table.getSelectionModel().getSelectedItem());
            }
        });
        hb.getChildren().add(sell);
        Button delete = new Button(TonyStark.getBundle("detail.page.function.delete"));
        delete.setOnAction(e -> {
            if (selectNotNull() && selectCountIsZero()) {
                ButtonType confirm = SFrameFX.alertMsgAndWait(
                        TonyStark.getBundle("detail.page.function.delete"),
                        TonyStark.getBundle("data.delete.confirm"));
                if (confirm == ButtonType.OK) {
                    DataSystem.removeStock(THIS_PANE, table.getSelectionModel().getSelectedItem().getId());
                    matchData();
                }
            }
        });
        hb.getChildren().add(delete);
        getChildren().addAll(table, hb);
    }

    private TableColumn<Stock, String> toColumn(String name, int minWidth, String type) {
        TableColumn<Stock, String> column = new TableColumn<>(name);
        column.setId(type);
        column.setMinWidth(minWidth);
        column.setCellValueFactory(
                new PropertyValueFactory<>(type));
        column.setCellFactory(TextFieldTableCell.forTableColumn());
        column.setOnEditCommit(
                t -> {
                    boolean save = true;
                    if (!type.equals(DATA_NAME)) {
                        if (!isNumber(t.getNewValue())) {
                            save = false;
                            SFrameFX.alertMsg(TonyStark.getBundle("data.error"), "unavailable number");
                        } else if (type.equals(DATA_COUNT) || type.equals(DATA_PRICE_BUY)) {
                            if(!isNoSignNumber(t.getNewValue())){
                                save = false;
                                SFrameFX.alertMsg(TonyStark.getBundle("data.error"), "unavailable number");
                            }
                        }
                    }

                    if (save) {
                        Stock stock = t.getTableView().getItems().get(
                                t.getTablePosition().getRow());
                        stock.setWithType(type, t.getNewValue());
                        updateStockProperty(THIS_PANE, stock);
                    }
                }
        );
        if (type.equals(DATA_ID)
                || type.equals(DATA_NAME)
                || type.equals(DATA_PRICE_YESTERDAY)
                || type.equals(DATA_PRICE_NOW)
                || type.equals(DATA_BUY_FEE)
                || type.equals(DATA_PRICE_OPEN))
            column.setEditable(false);
        return column;
    }

    private boolean selectNotNull() {
        if (table.getSelectionModel().getSelectedItem() == null) {
            SFrameFX.alertMsg(TonyStark.getBundle("data.error"), "Please select a stock first.");
        }
        return table.getSelectionModel().getSelectedItem() != null;
    }

    private boolean selectCountNotZero() {
        if (table.getSelectionModel().getSelectedItem().getCount().equals("0")) {
            SFrameFX.alertMsg(TonyStark.getBundle("data.error"), "Count is 0.");
        }
        return !table.getSelectionModel().getSelectedItem().getCount().equals("0");
    }

    private boolean selectCountIsZero() {
        if (!table.getSelectionModel().getSelectedItem().getCount().equals("0")) {
            SFrameFX.alertMsg(TonyStark.getBundle("data.error"), "Count is not 0.");
        }
        return table.getSelectionModel().getSelectedItem().getCount().equals("0");
    }

    public void matchData() {
        DataSystem.stockAsBean();
        table.setItems(DataSystem.getStockList());
    }
}
