package bin;

import data.DataSystem;
import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.SSLContext;
import java.io.*;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class AnueAPI {
    public static final String API_URL = "https://ws.api.cnyes.com/quote/api/v1/quotes/";
    public static final int timeout = 5; //second
    public static final String TSE01 = "TSE01";

    public static String getMultiStocks(Set idSet) {
        String[] ids = new String[idSet.size()];
        idSet.toArray(ids);
        return getMultiStocks(ids);
    }

    public static String getMultiStocks(String... ids) {
        String result;
        try {
            result = httpClientGet(getStockMap(ids));
        } catch (ConnectTimeoutException e) {
            return errorMessage(e.getCause().getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            return errorMessage(DataSystem.API_STATUS_FAIL);
        } catch (NoSuchAlgorithmException | KeyStoreException | KeyManagementException e) {
            e.printStackTrace();
            return errorMessage("SSL");
        }
        return resultAnalyze(result, ids);
    }

    private static String httpClientGet(String content) throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        String result = errorMessage(DataSystem.API_STATUS_FAIL);

        CloseableHttpClient httpClient = createAcceptSelfSignedCertificateClient();
        try {
            String url = API_URL + content;
            HttpGet request = new HttpGet(url);
            System.out.println(Calendar.getInstance().getTime());
            System.out.println("url:" + url);
            request.addHeader("accept", "*/*");
            request.addHeader("accept-encoding", "gzip, deflate, br");
            request.addHeader("accept-language", "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7");
            request.addHeader("sec-fetch-dest", "sec-fetch-dest");
            request.addHeader("sec-fetch-mode", "cors");
            request.addHeader("sec-fetch-site", "same-site");
            request.addHeader("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36");
            request.addHeader("x-cnyes-app", "stock");
            request.addHeader("x-platform", "WEB");
            request.addHeader("x-system-kind", "STOCK");

            CloseableHttpResponse response = httpClient.execute(request);
            try {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    // return it as a String
                    result = EntityUtils.toString(entity);
                }
            } finally {
                response.close();
            }
        } finally {
            httpClient.close();
        }
        System.out.println("result:" + result.trim());
        if (result.contains("503 Service")) result = errorMessage("http 503");
        return result;
    }

    private static CloseableHttpClient createAcceptSelfSignedCertificateClient()
            throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {

        SSLContext sslContext = SSLContextBuilder
                .create()
                .loadTrustMaterial(null, (TrustStrategy) (chain, authType) -> true)
                .build();

        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                sslContext,
                new String[]{"TLSv1.2"},
                null,
                SSLConnectionSocketFactory.getDefaultHostnameVerifier());

        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(timeout * 1000)
                .setConnectionRequestTimeout(timeout * 1000)
                .setSocketTimeout(timeout * 1000)
                .build();

        return HttpClients
                .custom()
                .setSSLSocketFactory(sslsf)
                .setDefaultRequestConfig(config)
                .build();
    }

    private static String getStockMap(String... stocks) {
        if (stocks.length <= 0) {
            throw new NegativeArraySizeException("stock can't be 0 or less");
        }
        StringBuilder stockStr = new StringBuilder();
        stockStr.append("TWS:").append(TSE01).append(":INDEX").append(",");
        for (String stock : stocks) {
            stockStr.append("TWS:").append(stock).append(":STOCK").append(",");
        }
        stockStr.setLength(stockStr.length() - 1);
        return stockStr.toString();
    }

    private static String resultAnalyze(String result, String... ids) {
        JSONObject stocksObject;
        try {
            stocksObject = JSONObject.fromObject(result);
        } catch (JSONException e) {
            e.printStackTrace();
            return errorMessage("result can't be analyze,\nresult:" + result);
        }
        if (stocksObject.isEmpty()) return errorMessage("stocksObject" + DataSystem.API_STATUS_EMPTY);
        if (!"200".equals(stocksObject.getString("statusCode"))) {
            return errorMessage(stocksObject.getString("message"));
        }
        JSONArray stocksArray = stocksObject.getJSONArray("data");
        if (stocksArray.isEmpty()) return errorMessage("array" + DataSystem.API_STATUS_EMPTY);

        JSONObject stocks = new JSONObject();
        for (Object stockJson : stocksArray) {
            JSONObject stockInfo = JSONObject.fromObject(stockJson);
            for (String id : ids) {
                if (stockInfo.getString("200010").contains(id)) {
                    stocks.put(id + DataSystem.API_STOCK_YESTERDAY, stockInfo.getString("21"));
                    stocks.put(id + DataSystem.API_STOCK_NAME, stockInfo.getString("200009"));
                    stocks.put(id + DataSystem.API_STOCK_OPEN, stockInfo.getString("19"));
                    stocks.put(id, stockInfo.getString("6"));
                } else if (stockInfo.getString("200010").equals(TSE01)) {
                    stocks.put(TSE01 + DataSystem.API_STOCK_AMPLITUDE, stockInfo.getString("11"));
                    stocks.put(TSE01, stockInfo.getString("6"));
                }
            }
        }

        if (stocks.isEmpty()) return errorMessage("stocks" + DataSystem.API_STATUS_EMPTY);
        return stocks.toString();
    }

    public static String errorMessage(String msg) {
        return DataSystem.API_STATUS_ERROR + msg;
    }

    public static boolean errorCheck(String msg) {
        return msg.contains(DataSystem.API_STATUS_ERROR);
    }


}
