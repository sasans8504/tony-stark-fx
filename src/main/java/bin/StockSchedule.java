package bin;

import data.DataSystem;
import javafx.application.Platform;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import panes.HomePane;

import java.util.Calendar;

import static data.DataSystem.*;

/**
 * 定時包含抓資料以及呼叫頁面更新數字
 * (僅更新部分，非整頁更新)
 * 1.because userData is static, and the userData is load by loadStock();
 */
public class StockSchedule implements Runnable {
    public static final int START_MINUTE = (60 * 8) + 20;
    public static final int END_MINUTE = (60 * 13) + 40;
    private final long SAVE_PERIOD = 60 * 30 * 1000;
    //    public static final int QUERY_PERIOD = 5; //shouldn't less than 5 (twse)
    public static final int QUERY_PERIOD = 30; //shouldn't less than 30 (anue)
    private long lastSaveTime;

    public StockSchedule() {
        lastSaveTime = SFrameFX.lastSaveTime;
    }

    @Override
    public void run() {
        if (SFrameFX.isNetworking() && timeCheck(SFrameFX.isForceRun())) {
            JSONObject userData = loadStock();
            if (!userData.isEmpty()) {
                String result = AnueAPI.getMultiStocks(userData.keySet());
                try {
                    JSONObject queryStocks = JSONObject.fromObject(result);
                    for (Object idSet : queryStocks.keySet()) {
                        String id = (String) idSet;
                        JSONObject stock;
                        if(id.contains(AnueAPI.TSE01)){
                            String inside = id;
                            if(id.contains(API_STOCK_AMPLITUDE)){
                                Platform.runLater(() -> SFrameFX.setTseAmp(queryStocks.getString(inside)));
                                continue;
                            }
                            Platform.runLater(() -> SFrameFX.setTseNow(queryStocks.getString(inside)));
                        }else {
                            if (id.contains(API_STOCK_OPEN)) {
                                id = id.split("_")[0];
                                stock = userData.getJSONObject(id);
                                stock.put(DATA_PRICE_OPEN, queryStocks.getString(id + API_STOCK_OPEN));
                                userData.put(id, stock);
                                continue;
                            } else if (id.contains(API_STOCK_NAME) || id.contains(API_STOCK_AMPLITUDE)) {
                                continue;
                            } else if (id.contains(API_STOCK_YESTERDAY)) {
                                id = id.split("_")[0];
                                stock = userData.getJSONObject(id);
                                stock.put(DATA_PRICE_YESTERDAY, queryStocks.getString(id + API_STOCK_YESTERDAY));
                                userData.put(id, stock);
                                continue;
                            }
                            stock = userData.getJSONObject(id);
                            stock.put(DATA_PRICE_NOW, queryStocks.getString(id));
                            alarmCheck(stock, id);
                            userData.put(id, stock); //*1
                        }
                    }
                    //refresh Stock
                    Platform.runLater(DataSystem::refreshStockList);

                    System.out.println("lastSaveTime:" + lastSaveTime);
                    if (lastSaveTime == 0 || System.currentTimeMillis() - lastSaveTime >= SAVE_PERIOD) {
                        saveUserData2All();
                        lastSaveTime = System.currentTimeMillis();
                        SFrameFX.firstQuote = false;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println("exception result:" + result);
                    Platform.runLater(() -> SFrameFX.timeRefresh(result));
                }
                if (!AnueAPI.errorCheck(result))
                    Platform.runLater(SFrameFX::timeRefresh);
            }
        }
    }

    private boolean timeCheck(boolean forceRun) {
        if (forceRun) {
            Platform.runLater(SFrameFX::switchForceRun);
            return true;
        }
        int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        hour *= 60;
        int minute = Calendar.getInstance().get(Calendar.MINUTE);
        minute += hour;
        return minute >= START_MINUTE && minute < END_MINUTE;
    }

    private void alarmCheck(JSONObject stock, String id) {
        if (stock.getBoolean(DATA_ALARM)) {
            if (!"0".equals(stock.getString(DATA_ALARM_LOW))) {
                if (!isLegal(id, DATA_ALARM_LOW, stock.getString(DATA_ALARM_LOW))) {
                    Platform.runLater(() -> HomePane.alarmMsg(TonyStark.getBundle("home.page.alarm.low"),
                            stock.getString(DATA_NAME)
                                    + TonyStark.getBundle("schedule.alarm") + TonyStark.getBundle("home.page.alarm.low") + "(" + stock.getString(DATA_ALARM_LOW) + ")\n"
                                    + TonyStark.getBundle("schedule.present.price") + ":" + stock.getString(DATA_PRICE_NOW), id));
                    setAlarmTo0(id, DATA_ALARM_LOW);
                    setAlarm(id, false, "StockSchedule low");
                }
            }
            if (!"0".equals(stock.getString(DATA_ALARM_HIGH))) {
                if (!isLegal(id, DATA_ALARM_HIGH, stock.getString(DATA_ALARM_HIGH))) {
                    Platform.runLater(() -> HomePane.alarmMsg(TonyStark.getBundle("home.page.alarm.high"),
                            stock.getString(DATA_NAME)
                                    + TonyStark.getBundle("schedule.alarm") + TonyStark.getBundle("home.page.alarm.high") + stock.getString(DATA_ALARM_HIGH) + ")\n"
                                    + TonyStark.getBundle("schedule.present.price") + ":" + stock.getString(DATA_PRICE_NOW), id));
                    setAlarmTo0(id, DATA_ALARM_HIGH);
                    setAlarm(id, false, "StockSchedule high");
                }
            }
        }
    }
}
