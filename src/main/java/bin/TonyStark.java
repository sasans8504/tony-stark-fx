package bin;

import data.DataSystem;
import javafx.application.Application;

import java.util.ResourceBundle;

public class TonyStark {
    public static ResourceBundle bundle;
    public static void main(String[] args) {
        DataSystem.INSTANCE.init();
        bundle = ResourceBundle.getBundle(DataSystem.getSystemSetting(DataSystem.DATA_SYSTEM_LANGUAGE));
        bin.STray.getInstance();
        Application.launch(SFrameFX.class);
    }

    public static String getBundle(String key){
        return bundle.getString(key);
    }

    public static void switchBundle(String language){
        bundle = ResourceBundle.getBundle(language);
    }
}
