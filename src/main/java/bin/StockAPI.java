package bin;

import data.DataSystem;
import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.SSLContext;
import java.io.*;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class StockAPI {
    public static final String getStockInfoJsp = "https://mis.twse.com.tw/stock/api/getStockInfo.jsp";
    public static final int timeout = 3; //second

    public static String getMultiStocks(Set idSet) {
        String[] ids = new String[idSet.size()];
        idSet.toArray(ids);
        return getMultiStocks(ids);
    }

    public static String getMultiStocks(String... ids) {
        String result;
        try {
            result = httpClientGet(getStockMap(ids));
        } catch (ConnectTimeoutException e){
            return errorMessage(e.getCause().getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            return errorMessage(DataSystem.API_STATUS_FAIL);
        } catch (NoSuchAlgorithmException | KeyStoreException | KeyManagementException e) {
            e.printStackTrace();
            return errorMessage("SSL");
        }
        return resultAnalyze(result, ids);
    }

    private static String httpClientGet(Map<String, String> stockMap) throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        String result = errorMessage(DataSystem.API_STATUS_FAIL);
        String content = getUrl(stockMap);

        CloseableHttpClient httpClient = createAcceptSelfSignedCertificateClient();
        try {
            String url = getStockInfoJsp + content;
            HttpGet request = new HttpGet(url);
            System.out.println(Calendar.getInstance().getTime());
            System.out.println("url:" + url);
            request.addHeader("Accept", "application/json, text/javascript, */*; q=0.01");
            request.addHeader("Accept-Encoding", "gzip, deflate, br");
            request.addHeader("Accept-Language", "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7");
            request.addHeader("Connection", "keep-alive");
            request.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36");
            request.addHeader("X-Requested-With", "XMLHttpRequest");

            CloseableHttpResponse response = httpClient.execute(request);
            try {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    // return it as a String
                    result = EntityUtils.toString(entity);
                }
            } finally {
                response.close();
            }
        } finally {
            httpClient.close();
        }
        System.out.println("result:"+result.trim());
        if(result.contains("503 Service")) result = errorMessage("http 503");
        return result;
    }

    private static String getUrl(Map<String, String> map) {
        if (map.isEmpty()) return "";
        StringBuilder sb = new StringBuilder();
        sb.append("?");
        for (String key : map.keySet()) {
            sb.append(key).append("=").append(map.get(key)).append("&");
        }
        String strURL = sb.toString();
        if ("&".equals(""
                + strURL.charAt(strURL.length() - 1))) {
            strURL = strURL.substring(0, strURL.length() - 1);
        }
        return strURL;
    }

    private static CloseableHttpClient createAcceptSelfSignedCertificateClient()
            throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {

        SSLContext sslContext = SSLContextBuilder
                .create()
                .loadTrustMaterial(null, (TrustStrategy) (chain, authType) -> true)
                .build();

        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                sslContext,
                new String[] { "TLSv1.2" },
                null,
                SSLConnectionSocketFactory.getDefaultHostnameVerifier());

        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(timeout * 1000)
                .setConnectionRequestTimeout(timeout * 1000)
                .setSocketTimeout(timeout * 1000)
                .build();

        return HttpClients
                .custom()
                .setSSLSocketFactory(sslsf)
                .setDefaultRequestConfig(config)
                .build();
    }

    private static Map<String, String> getStockMap(String... stocks) {
        if (stocks.length <= 0) {
            throw new NegativeArraySizeException("stock can't be 0 or less");
        }
        StringBuilder stockStr = new StringBuilder();
        for (String stock : stocks) {
            stockStr.append("tse_").append(stock).append(".tw").append("|");
        }
        stockStr.setLength(stockStr.length() - 1);
        Map<String, String> stockMap = new HashMap<>();
        try {
            stockMap.put("ex_ch", URLEncoder.encode(stockStr.toString(), "utf-8"));
        }catch (UnsupportedEncodingException e){
            e.printStackTrace();
            System.out.println("#error encode unsupported");
        }
        stockMap.put("json", "1");
        stockMap.put("delay", "0");
        stockMap.put("_", String.valueOf(System.currentTimeMillis()));
        return stockMap;
    }

    /**
     * should be update if reuse.
     */
    private static String resultAnalyze(String result, String... ids) {
        JSONObject stocksObject;
        try {
            stocksObject = JSONObject.fromObject(result);
        } catch (JSONException e) {
            e.printStackTrace();
            return errorMessage("result can't be analyze,\nresult:" + result);
        }
        if (stocksObject.isEmpty()) return errorMessage("stocksObject" + DataSystem.API_STATUS_EMPTY);
        if (!"0000".equals(stocksObject.getString("rtcode"))) {
            return errorMessage(stocksObject.getString("rtmessage"));
        }
        JSONArray stocksArray = stocksObject.getJSONArray("msgArray");
        if (stocksArray.isEmpty()) return errorMessage("array" + DataSystem.API_STATUS_EMPTY);

        JSONObject stocks = new JSONObject();
        for (Object stockJson : stocksArray) {
            JSONObject stockInfo = JSONObject.fromObject(stockJson);
            for (String id : ids) {
                if (stockInfo.getString("ch").contains(id)) {
                    stocks.put(id, stockInfo.getString("z"));
                }
            }
        }

        if (stocks.isEmpty()) return errorMessage("stocks" + DataSystem.API_STATUS_EMPTY);
        return stocks.toString();
    }

    public static String errorMessage(String msg) {
        return DataSystem.API_STATUS_ERROR + msg;
    }

    public static boolean errorCheck(String msg) {
        return msg.contains(DataSystem.API_STATUS_ERROR);
    }


}
