package bin;

import data.DataSystem;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import panes.*;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class SFrameFX extends Application {
    ScheduledExecutorService networkService = Executors.newSingleThreadScheduledExecutor();
    ScheduledFuture<?> networkSchedule;
    final int WIDTH = 700;
    final int HEIGHT = 400 + 50;
    private static Set<Stage> otherStages = new HashSet<>();
    private static Label refreshTime;
    private static Label failMsg;
    private static Label tseNow;
    private static Label tseAmp;
    private static String tseNowText = "";
    private static String tseAmpText = "";
    public static long lastSaveTime = 0;
    public static boolean firstQuote = false;
    private static Stage primaryStage;
    private static boolean networking = true;
    private static boolean forceRun = true;
    private static BorderPane mainPane;

    @Override
    public void start(Stage primaryStage) {
        SFrameFX.primaryStage = primaryStage;

        mainPane = new BorderPane();
        loginPage();

        Scene scene = new Scene(mainPane, WIDTH, HEIGHT);
        primaryStage.setTitle(TonyStark.getBundle("primary.stage.title"));
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setOnCloseRequest(e -> primaryStage.hide());
        Platform.setImplicitExit(false);
    }

    private void loginPage() {
        STray.lockMenu();
        checkSystemRunning();
        disposePane();

        GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);
        pane.add(new Label(TonyStark.getBundle("login.page.user")), 0, 0);
        TextField user = new TextField();
        pane.add(user, 1, 0);
        Button login = new Button(TonyStark.getBundle("login.page.login"));
        pane.add(login, 1, 1);
        GridPane.setHalignment(login, HPos.RIGHT);
        EventHandler<ActionEvent> loginEvent = e -> {
            if (DataSystem.isNumberOrAlpha(user.getText())) {
                DataSystem.loadUser(user.getText());
                mainPage();
            } else {
                alertMsg(TonyStark.getBundle("data.error"), "Please type legal user name");
            }
        };
        user.setOnAction(loginEvent);
        login.setOnAction(loginEvent);

        Set users = DataSystem.getAllUser();
        if (!users.isEmpty()) {
            HBox top = new HBox(10);
            top.setPadding(new Insets(10, 10, 10, 10));
            top.setStyle("-fx-border-color: gray");
            Label note = new Label(TonyStark.getBundle("login.page.quick"));
            note.setFont(Font.font(DataSystem.SYS_FONT, FontWeight.EXTRA_BOLD, FontPosture.ITALIC, 18));
            top.getChildren().add(note);
            for (Object account : users) {
                Button button = new Button(account.toString());
                button.setOnAction(e -> {
                    DataSystem.loadUser(account.toString());
                    mainPage();
                });
                top.getChildren().add(button);
            }
            mainPane.setTop(top);
        }

        HBox bottom = new HBox(3);
        bottom.setPadding(new Insets(10, 10, 10, 10));
        bottom.setStyle("-fx-border-color: gray");
        bottom.setAlignment(Pos.CENTER_RIGHT);
        Label lan = new Label(TonyStark.getBundle("login.page.language"));
        lan.setFont(Font.font(DataSystem.SYS_FONT, FontWeight.EXTRA_BOLD, FontPosture.ITALIC, 18));
        ObservableList<String> languages = FXCollections.observableArrayList(DataSystem.DATA_LANGUAGE_TW, DataSystem.DATA_LANGUAGE_US);
        ComboBox<String> language = new ComboBox<>(languages);
        language.setValue(DataSystem.getSystemSetting(DataSystem.DATA_SYSTEM_LANGUAGE));
        language.setOnAction(event -> {
            DataSystem.setSystemSetting(DataSystem.DATA_SYSTEM_LANGUAGE, language.getValue());
            TonyStark.switchBundle(language.getValue());
            loginPage();
        });
        bottom.getChildren().addAll(lan, language);

        mainPane.setCenter(pane);
        mainPane.setBottom(bottom);
    }


    private void mainPage() {
        DataSystem.stockAsBean();
        STray.unlockMenu();
        HBox pages = new HBox(5);
        Button homePage = new Button(TonyStark.getBundle("tray.home.page"));
        homePage.setOnAction(e -> mainPane.setCenter(HomePane.getInstance()));
        Button detailPage = new Button(TonyStark.getBundle("tray.detail.page"));
        detailPage.setOnAction(e -> mainPane.setCenter(DetailPane.getInstance()));
        Button historyPage = new Button(TonyStark.getBundle("tray.history.page"));
        historyPage.setOnAction(e -> mainPane.setCenter(HistoryPane.getInstance()));
        Button settingPage = new Button(TonyStark.getBundle("tray.setting.page"));
        settingPage.setOnAction(e -> mainPane.setCenter(SettingPane.getInstance()));
        pages.getChildren().addAll(homePage, detailPage, historyPage, settingPage);

        HBox times = new HBox(5);
        times.setAlignment(Pos.CENTER_RIGHT);
        refreshTime = new Label(TonyStark.getBundle("main.page.last.update") + Calendar.getInstance().getTime().toString());
        failMsg = new Label();
        Button logOut = new Button(TonyStark.getBundle("main.page.logout"));
        logOut.setOnAction(e -> {
            ButtonType confirm = SFrameFX.alertMsgAndWait(TonyStark.getBundle("main.page.logout"), TonyStark.getBundle("main.page.logout.confirm"));
            if (confirm == ButtonType.OK) {
                networkSchedule.cancel(true);
                loginPage();
            }
        });
        times.getChildren().addAll(failMsg, refreshTime, logOut);
        HBox tse01 = new HBox(10);
        tse01.setAlignment(Pos.CENTER_LEFT);
        HBox nowBox = new HBox(3);
        nowBox.setAlignment(Pos.CENTER);
        tseNow = new Label(tseNowText);
        nowBox.getChildren().addAll(new Label(TonyStark.getBundle("main.page.tse.now")), tseNow);
        HBox ampBox = new HBox(3);
        ampBox.setAlignment(Pos.CENTER);
        tseAmp = new Label(tseAmpText);
        ampBox.getChildren().addAll(new Label(TonyStark.getBundle("main.page.tse.amp")), tseAmp);
        tse01.getChildren().addAll(nowBox, ampBox);
        setTseColor(tseAmpText);

        BorderPane bottom = new BorderPane();
        bottom.setLeft(tse01);
        bottom.setRight(times);

//        Button debugWindow = new Button(TonyStark.getBundle("tray.developer.page"));
//        debugWindow.setOnAction(e -> new DebugStage());
//        pages.getChildren().add(debugWindow);

        mainPane.setTop(pages);
        mainPane.setCenter(HomePane.getInstance());
        mainPane.setBottom(bottom);

        networkSchedule = networkService.scheduleWithFixedDelay(new StockSchedule(), 0, StockSchedule.QUERY_PERIOD, TimeUnit.SECONDS);
    }

    public static void showStage() {
        primaryStage.show();
    }

    public static void showStage(Pane pane) {
        showStage();
        mainPane.setCenter(pane);
    }

    public static void switchNetworking() {
        SFrameFX.networking = !SFrameFX.networking;
        System.out.println("network:" + SFrameFX.networking);
    }

    public static boolean isNetworking() {
        return networking;
    }

    public static void switchForceRun() {
        SFrameFX.forceRun = !SFrameFX.forceRun;
        System.out.println("forceRun:" + SFrameFX.forceRun);
    }

    public static boolean isForceRun() {
        return forceRun;
    }

    public static void timeRefresh() {
        timeRefresh("");
    }

    public static void timeRefresh(String msg) {
        refreshTime.setText(TonyStark.getBundle("main.page.last.update") + Calendar.getInstance().getTime().toString());
        failMsg.setText(msg);
    }

    public static void setTseNow(String now) {
        tseNowText = now;
        tseNow.setText(now);
    }

    public static void setTseAmp(String amp) {
        tseAmpText = amp;
        tseAmp.setText(amp);
        setTseColor(amp);
    }

    private static void setTseColor(String amp) {
        if ("".equals(amp)) return;

        double ampD = Double.parseDouble(amp);
        double ans = Math.signum(ampD);
        if (ans > 0) {
            tseNow.setTextFill(Color.RED);
            tseAmp.setTextFill(Color.RED);
        } else if (ans < 0) {
            tseNow.setTextFill(Color.GREEN);
            tseAmp.setTextFill(Color.GREEN);
        } else {
            tseNow.setTextFill(Color.BLACK);
            tseAmp.setTextFill(Color.BLACK);
        }
    }

    public static void registeredStage(Stage stage) {
        otherStages.add(stage);
    }

    public static int countStages() {
        return otherStages.size();
    }

    public static void removeStage(Stage stage) {
        otherStages.remove(stage);
        stage.close();
    }

    private void checkSystemRunning() {
        if (!otherStages.isEmpty()) {
            for (Stage stage : otherStages) {
                stage.close();
            }
            otherStages.clear();
        }
        lastSaveTime = 0;
    }

    public static ButtonType alertMsgAndWait(String title, String msg) {
        return alertMsgAndWait(title, msg, Alert.AlertType.CONFIRMATION);
    }

    public static ButtonType alertMsgAndWait(String title, String msg, Alert.AlertType type) {
        Alert alert = new Alert(type);
        alert.initOwner(primaryStage);
        alert.setTitle(title);
        alert.setHeaderText(title);
        alert.setContentText(msg);
        return alert.showAndWait().get();
    }

    public static void alertMsg(String title, String msg) {
        alertMsg(title, msg, Alert.AlertType.INFORMATION);
    }

    public static void alertMsg(String title, String msg, Alert.AlertType type) {
        Alert alert = new Alert(type);
        alert.initOwner(primaryStage);
        alert.setTitle(title);
        alert.setHeaderText(title);
        alert.setContentText(msg);
        alert.show();
    }

    private void disposePane() {
        HomePane.dispose();
        DetailPane.dispose();
        HistoryPane.dispose();
        SettingPane.dispose();
    }
}