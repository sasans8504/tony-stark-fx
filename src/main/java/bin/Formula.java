package bin;

import data.SoldStock;
import data.Stock;
import javafx.scene.paint.Color;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Formula {
    private static String broker_discount = "65"; // percent (%)
    private static String fee_rate = "0.001425"; //percent (%)
    private static String transfer_tax_rate = "0.003"; //percent (%)
    private static String lowest_fee = "20";
    private static String zero = "0";
    private static BigDecimal bd_tax = new BigDecimal(transfer_tax_rate);
    private static BigDecimal bd_thousand = new BigDecimal("1000");
    private static BigDecimal bd_fee_rate = new BigDecimal(fee_rate);
    private static BigDecimal bd_lowest_fee = new BigDecimal(lowest_fee);
    private static BigDecimal bd_broker_discount = new BigDecimal(broker_discount).divide(new BigDecimal("100"), 2, BigDecimal.ROUND_HALF_UP);
    public static final String NEW_PRICE = "new_price";
    public static final String NEW_COUNT = "new_count";
    public static final String NEW_FEE = "new_fee";

    public static Color isRed(Stock stock) {
        BigDecimal now = new BigDecimal(stock.getPrice_now());
        BigDecimal ysclose = new BigDecimal(stock.getPrice_ysclose());
        if (now.compareTo(ysclose) > 0) return Color.RED;
        else if (now.compareTo(ysclose) == 0) return Color.BLACK;
        else return Color.GREEN;
    }

    public static Map<String, String> merge(Stock stock, String price, String count) {
        Map<String, String> result = new HashMap<>();
        if (count.equals(zero)) {
            result.put(NEW_PRICE, price);
            result.put(NEW_COUNT, count);
            result.put(NEW_FEE, zero);
            return result;
        }
        BigDecimal originPrice = new BigDecimal(stock.getPrice_buy());
        BigDecimal originCount = new BigDecimal(stock.getCount());
        BigDecimal mergePrice = new BigDecimal(price);
        BigDecimal mergeCount = new BigDecimal(count);
        String newPrice = ((originPrice.multiply(originCount))
                .add((mergePrice.multiply(mergeCount))))
                .divide(originCount.add(mergeCount), 2, BigDecimal.ROUND_HALF_UP)
                .toPlainString();
        String newCount = originCount.add(mergeCount).toString();
        String newFee = mergeFee(stock, fee(price, count));

        result.put(NEW_PRICE, newPrice);
        result.put(NEW_COUNT, newCount);
        result.put(NEW_FEE, newFee);
        return result;
    }

    public static String mergeFee(Stock stock, String newFee) {
        if (stock.getBuy_fee().equals(zero) && newFee.equals(zero)) return zero;
        return new BigDecimal(stock.getBuy_fee()).add(new BigDecimal(newFee)).toPlainString();
    }

    public static String fee(String price, String count) {
        if (count.equals(zero)) return zero;

        BigDecimal buyPrice = new BigDecimal(price);
        BigDecimal buyCount = new BigDecimal(count);
        BigDecimal result = buyPrice.multiply(bd_thousand)
                .multiply(buyCount)
                .multiply(bd_fee_rate)
                .multiply(bd_broker_discount)
                .setScale(2, BigDecimal.ROUND_HALF_UP);
        if (result.compareTo(bd_lowest_fee) < 0) return lowest_fee;
        else return result.toPlainString();
    }

    public static String splitFee(Stock stock, String sellCount) {
        BigDecimal fee = new BigDecimal(stock.getBuy_fee());
        BigDecimal own = new BigDecimal(stock.getCount());
        BigDecimal sell = new BigDecimal(sellCount);
        BigDecimal percent = sell.divide(own, 2, BigDecimal.ROUND_HALF_UP);
        System.out.println(stock.getBuy_fee() + " * (" + sellCount + " / " + stock.getCount() + ")");
        return fee.multiply(percent)
                .setScale(2, BigDecimal.ROUND_HALF_UP)
                .toPlainString();
    }

    public static String tax(Stock stock, String sellCount) {
        if (stock.getPrice_now().equals(zero)) return zero;

        return new BigDecimal(stock.getPrice_now())
                .multiply(bd_thousand)
                .multiply(new BigDecimal(sellCount))
                .multiply(bd_tax)
                .setScale(2, BigDecimal.ROUND_HALF_UP)
                .toPlainString();
    }

    public static String revenue(SoldStock soldStock) {
        BigDecimal market = new BigDecimal(soldStock.getPrice_market());
        BigDecimal sold = new BigDecimal(soldStock.getPrice_sold());
        BigDecimal count = new BigDecimal(soldStock.getCount());
        BigDecimal buyFee = new BigDecimal(soldStock.getBuy_fee());
        BigDecimal tax = new BigDecimal(soldStock.getTax());
        BigDecimal soldFee = new BigDecimal(soldStock.getSold_fee());
        return (market.multiply(bd_thousand).multiply(count))
                .subtract((sold.multiply(bd_thousand).multiply(count)))
                .subtract(buyFee)
                .subtract(tax)
                .subtract(soldFee)
                .toPlainString();
    }

    public static String getBroker_discount() {
        return broker_discount;
    }

    public static void setBroker_discount(String broker_discount) {
        Formula.broker_discount = broker_discount;
        bd_broker_discount = new BigDecimal(broker_discount).divide(new BigDecimal("100"), 2, BigDecimal.ROUND_HALF_UP);
    }

    public static String getFee_rate() {
        return fee_rate;
    }

    public static void setFee_rate(String fee_rate) {
        Formula.fee_rate = fee_rate;
    }

    public static String getTransfer_tax_rate() {
        return transfer_tax_rate;
    }

    public static void setTransfer_tax_rate(String transfer_tax_rate) {
        Formula.transfer_tax_rate = transfer_tax_rate;
    }

    public static String getLowest_fee() {
        return lowest_fee;
    }

    public static void setLowest_fee(String lowest_fee) {
        Formula.lowest_fee = lowest_fee;
    }
}
