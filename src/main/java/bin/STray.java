package bin;

import javafx.application.Platform;
import panes.DetailPane;
import panes.HistoryPane;
import panes.HomePane;
import panes.SettingPane;
import stage.DebugStage;

import java.awt.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class STray {
    private static STray sTray;
    public static STray getInstance(){
        if(sTray == null) sTray = new STray();
        return sTray;
    }
    private static Set<MenuItem> lockMenu = new HashSet<>();

    SystemTray systemTray;
    TrayIcon trayIcon;

    STray(){
        if (SystemTray.isSupported()) {
            systemTray = SystemTray.getSystemTray();
            Image trayIconImage = Toolkit.getDefaultToolkit().getImage(Objects.requireNonNull(STray.class.getClassLoader().getResource("icon.png")));
            PopupMenu popupMenu = new PopupMenu();
            MenuItem homePage = new MenuItem(TonyStark.getBundle("tray.home.page"));
            homePage.addActionListener(e -> Platform.runLater(() -> SFrameFX.showStage(HomePane.getInstance())));
            MenuItem detailPage = new MenuItem(TonyStark.getBundle("tray.detail.page"));
            detailPage.addActionListener(e -> Platform.runLater(() -> SFrameFX.showStage(DetailPane.getInstance())));
            MenuItem historyPage = new MenuItem(TonyStark.getBundle("tray.history.page"));
            historyPage.addActionListener(e -> Platform.runLater(() -> SFrameFX.showStage(HistoryPane.getInstance())));
            MenuItem settingPage = new MenuItem(TonyStark.getBundle("tray.setting.page"));
            settingPage.addActionListener(e -> Platform.runLater(() -> SFrameFX.showStage(SettingPane.getInstance())));
            MenuItem developer = new MenuItem(TonyStark.getBundle("tray.developer.page"));
            developer.addActionListener(e -> Platform.runLater((DebugStage::new)));
            MenuItem exitItem = new MenuItem(TonyStark.getBundle("tray.exit.item"));
            exitItem.addActionListener(e -> {
                systemTray.remove(trayIcon);
                System.exit(0);
            });
            lockMenu.add(homePage);
            lockMenu.add(detailPage);
            lockMenu.add(historyPage);
            lockMenu.add(settingPage);
            lockMenu.add(developer);

            popupMenu.add(homePage);
            popupMenu.add(detailPage);
            popupMenu.add(historyPage);
            popupMenu.add("-");
            popupMenu.add(developer);
            popupMenu.add("-");
            popupMenu.add(settingPage);
            popupMenu.add("-");
            popupMenu.add(exitItem);

            trayIcon = new TrayIcon(trayIconImage, TonyStark.getBundle("tray.icon"), popupMenu);
            trayIcon.setImageAutoSize(true);
            trayIcon.addActionListener(e -> Platform.runLater(SFrameFX::showStage));
            try {
                systemTray.add(trayIcon);
            } catch (AWTException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println(TonyStark.getBundle("tray.not.support"));
            System.exit(0);
        }
    }

    public static void lockMenu(){
        for(MenuItem page: lockMenu){
            page.setEnabled(false);
        }
    }

    public static void unlockMenu(){
        for(MenuItem page: lockMenu){
            page.setEnabled(true);
        }
    }
}
